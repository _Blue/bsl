#include "TCPTester.h"

#define TEST_PORT 1350

using namespace BSL::Network;
using namespace std;

namespace BSL
{
    namespace Test
    {
        TCPTester::TCPTester() : Tester(), ClientHandler()
        {

        }

        TCPTester::~TCPTester()
        {
            delete client;
            delete server;
        }

        void TCPTester::Start()
        {
            thread thread(&TCPTester::Listen, this);
            client = new TCPClient();
            Test(client->Connect("127.0.0.1", TEST_PORT) == TCPError::Sucess, "TCPClient connection");
            thread.join();
            uint port = client->GetHighConnectionPort();
            Test(port != 0 && 1350 == server->GetHighConnectionPort(), "Connection port validity");
            Packet(reinterpret_cast<const BYTE*>("foo"), 3);
            auto bigpacket = GenerateData(100000);//Test very big packet
            Packet(bigpacket.data(), 100000);
        }

        void TCPTester::Listen()
        {
            TCPServer tcpserver;
            Test(tcpserver.Start(TEST_PORT, *this, "127.0.0.1") == TCPError::Sucess, "TCPServer connection");
        }

        bool TCPTester::ClientConnected(const ClientInfos &infos)
        {
            server = new TCPBase(infos.socket);
            return false;
        }

        void TCPTester::Packet(const BYTE *data, UINT32 size)
        {
            Test(client->Send(data, size) == TCPError::Sucess, "Packet send");

            SecuredVector<BYTE> buffer;
            Test(server->Receive(buffer, size) == TCPError::Sucess, "Packet receive");

            Test(buffer.size() == size && memcmp(data, buffer.data(), size) == 0, "Data integrity");

            Test(server->Send(data, size) == TCPError::Sucess, "Packet send");
            Test(client->Receive(buffer, size) == TCPError::Sucess, "Packet receive");
            Test(buffer.size() == size && memcmp(data, buffer.data(), size) == 0, "Data integrity");
        }
    }
}