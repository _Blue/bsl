#include "stdafx.h"
#include "Tester.h"

using namespace std;
namespace BSL
{
    namespace Test
    {
        Tester::Tester()
        {

        }

        void Tester::Test(bool success, const char *message)
        {
            if (!success)
            {
                cout << "Failure on \"" << message << "\"" << endl;
                if (IsDebuggerPresent())//Check if a debugger is attached
                {
                    __debugbreak();// Trigger breakpoint on test failure
                }
            }
        }

        vector<byte> Tester::GenerateData(size_t size)
        {
            vector<byte> ret;
            ret.resize(size);
            for (size_t i= 0; i < size; i++)
            {
                ret[i] = rand() % 256;
            }
            return ret;
        }
    }
}