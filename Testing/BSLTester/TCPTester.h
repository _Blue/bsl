#include "stdafx.h"
#include "Tester.h"
#include "../../bsl/Network/TCPServer.h"
#include "../../bsl/Network/TCPClient.h"
namespace BSL
{
    namespace Test
    {
        class TCPTester : protected Tester, Network::ClientHandler
        {
        public:
            TCPTester();
            ~TCPTester();
            void Start();

            bool ClientConnected(const Network::ClientInfos &infos) override;
            void Listen();

        private:
            Network::TCPClient *client;
            Network::TCPBase *server;
            void Packet(const BYTE *data, UINT32 size);
        };
    }
}