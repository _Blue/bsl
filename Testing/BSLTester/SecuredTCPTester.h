#pragma once

#include "Tester.h"
#include "../../bsl/Network/SecuredTCPClient.h"
#include "../../bsl/Network/TCPServer.h"

namespace BSL
{
    namespace Test
    {
        class SecuredTCPTester : protected Tester, protected Network::ClientHandler
        {
        public:
            SecuredTCPTester() = default;

            void Start();

        private:
            void CheckOptions(const Network::SecuredTCPOptions &options);
            void TestReadWrite();
            std::pair<std::unique_ptr<Network::SecuredTCPClient>, std::unique_ptr<Network::SecuredTCPClient>> CreateConnection(const Network::SecuredTCPOptions &options);
            bool ClientConnected(const Network::ClientInfos &infos) override;
            void TestExchange(Network::SecuredTCP &client, Network::SecuredTCP &server);
            void StartServer(Network::SecuredTCPClient *server);
            void TestPacketEncoding(Crypto::SymetricEncryption alg, Crypto::HashFunction hash, const SecuredVector<byte> &secret);
            Network::ClientInfos _clientinfos;
            Network::SecuredTCPOptions _options;
        };
    }
}