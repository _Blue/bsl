#include "stdafx.h"
#include "CryptoTester.h"
#include "TCPTester.h"
#include "SecuredTCPTester.h"
#include "Win32Tester.h"
using namespace std;
using namespace BSL::Test;
using namespace BSL;

int main()
{
    cout << "Starting tests" << endl;

    Win32Tester win32tester;
    win32tester.Start();
    cout << "Win32: OK" << endl;

    CryptoTester ctester;
    ctester.Start();
    cout << "Crypto: OK" << endl;

    TCPTester tcptester;
    tcptester.Start();
    cout << "TCP: OK" << endl;

    SecuredTCPTester stester;
    stester.Start();
    cout << "SecuredTCP: OK" << endl;

    cout << "Looks like each and every test succeeded, well done" << endl;
    return 0;
}