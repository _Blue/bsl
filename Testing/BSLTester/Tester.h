#pragma once

#include <vector>
#include "../../bsl/BSLDefs.h"
namespace BSL
{
    namespace Test
    {
        class Tester
        {
        public:
            Tester();

        protected:
            void Test(bool success, const char *name);
            std::vector<byte> GenerateData(size_t size);

        };
    }
}