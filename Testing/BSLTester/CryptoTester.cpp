#include "CryptoTester.h"
#include "../../bsl/Crypto/Encoding.h"
#include "../../bsl/Crypto/MD5.h"
#include "../../bsl/Crypto/SHA1.h"
#include "../../bsl/Crypto/SHA256.h"
#include "../../bsl/Crypto/RC4Encryptor.h"
#include "../../bsl/Crypto/CryptoProvider.h"

using namespace BSL::Crypto;
using namespace std;

const BYTE *smalldata = reinterpret_cast<BYTE*>("AllINeedIsFoo");
const BYTE *keyderival = reinterpret_cast<BYTE*>("AllIHaveIsBar");

namespace BSL
{
    namespace Test
    {
        CryptoTester::CryptoTester() : Tester()
        {

        }

        void CryptoTester::Start()
        {
            EncodingTest();

            //Test asymetric encryptions
            auto asymetric = CryptoProvider::GetSupportedAsymetricEncryptions();
            for (auto e : asymetric)
            {
                auto cryptor = CryptoProvider::Construct(e);
                Test(cryptor->GenerateKey(CryptoProvider::GetDefaultKeySize(e)), "Asymetric key generation");
                GeneralTest(GenerateData(2048), *cryptor);
                AsymetricTest(GenerateData(2048), *cryptor);
            }

            //Test symetric encryptions
            auto symetric = CryptoProvider::GetSupportedSymetricEncryptions();
            for (auto e : symetric)
            {
                //Test random key generation
                auto cryptor = CryptoProvider::Construct(e);
                Test(cryptor->GenerateKey(CryptoProvider::GetDefaultKeySize(e)), "Symetric key generation");
                GeneralTest(GenerateData(2048), *cryptor);
                SymetricTest(GenerateData(2048), *cryptor);

                //Test key derivation
                cryptor = CryptoProvider::Construct(e);
                auto other = CryptoProvider::Construct(e);
                auto keybase = GenerateData(2048);
                Test(cryptor->DeriveKey(keybase, CryptoProvider::GetDefaultKeySize(e)), "Key derivation");
                Test(other->DeriveKey(keybase, CryptoProvider::GetDefaultKeySize(e)), "Key derivation");

                //Check that two keys derived with the same base are equals
                SecuredVector<byte> cryptorkey;
                Test(cryptor->GetKey(cryptorkey), "Key export");
                SecuredVector<byte> otherkey;
                Test(other->GetKey(otherkey), "Key export");
                Test(otherkey == cryptorkey, "Key derivation validity");
                SymetricTest(GenerateData(2048), *cryptor);
            }
            vector<BYTE> keybase;
            keybase.insert(keybase.begin(), keyderival, keyderival + strlen(reinterpret_cast<const char*>(keyderival)));

            RC4Encryptor rc4cryptor;
            Test(rc4cryptor.GenerateKey(128), "RC4 key generation");
            TestClass(rc4cryptor);
            Test(rc4cryptor.DeriveKey(keybase, 128), "RC4 key derivation");
            TestClass(rc4cryptor);

            RSAEncryptor rsacryptor;
            Test(rsacryptor.GenerateKey(2048), "RSA Key generation");
            TestClass(rsacryptor);

            AESEncryptor aescryptor;
            Test(aescryptor.GenerateKey(256), "AES Key generation");
            TestClass(aescryptor);
            Test(aescryptor.DeriveKey(keybase, 256), "AES key derivation");
            TestClass(rsacryptor);

            HashTest();

            TestFactory();
        }

        void CryptoTester::TestClass(AsymetricEncryptor &cryptor)
        {
            SecuredVector<BYTE> data(smalldata, smalldata + strlen((char*)smalldata));
            AsymetricTest(data, cryptor);
            data = GenerateData(16000);
            AsymetricTest(data, cryptor);
            AsymetricTest(data, move(cryptor));//Check move constructor
        }

        void CryptoTester::TestClass(SymetricEncryptor &cryptor)
        {
            SecuredVector<BYTE> data(smalldata, smalldata + strlen((char*)smalldata));
            SymetricTest(data, cryptor);
            data = GenerateData(16000);
            SymetricTest(data, cryptor);
            SymetricTest(data, move(cryptor));//Check move constructor
        }

        void CryptoTester::GeneralTest(const SecuredVector<BYTE> &data, CryptoBase &cryptor)
        {
            //Test simple encryption / decryption
            SecuredVector<BYTE> datacopy(data.begin(), data.end());
            Test(cryptor.Encrypt(datacopy), "Data encryption");
            Test(cryptor.Decrypt(datacopy), "Data decryption");
            Test(data.size() == datacopy.size() && memcmp(data.data(), datacopy.data(), data.size()) == 0, "Data validity");
        }

        void CryptoTester::AsymetricTest(const SecuredVector<BYTE> &data, AsymetricEncryptor &cryptor)
        {
            SecuredVector<BYTE> datacopy(data.begin(), data.end());

            //Test private key export / import
            SecuredVector<BYTE> key;
            Test(cryptor.GetPrivateKey(key), "Private key export");
            RSAEncryptor extcryptor;
            Test(extcryptor.SetPrivateKey(key), "Private key import");
            Test(extcryptor.Encrypt(datacopy), "encryption");
            Test(extcryptor.Decrypt(datacopy), "decryption");
            Test(data.size() == datacopy.size() && memcmp(data.data(), datacopy.data(), data.size()) == 0, "Data validity");

            //Test public key import / export

            key.clear();//Make sure that previous key won't interfere
            Test(cryptor.GetPublicKey(key), "Public key export");

            auto publiccryptor = CreateAnother(cryptor);
            Test(publiccryptor->SetPublicKey(key), "Public key import");
            Test(publiccryptor->Encrypt(datacopy), "public key encryption");
            Test(cryptor.Decrypt(datacopy), "Should never fail");
            Test(data.size() == datacopy.size() && memcmp(data.data(), datacopy.data(), data.size()) == 0, "Data validity");
        }

        void CryptoTester::SymetricTest(const SecuredVector<BYTE> &data, SymetricEncryptor &cryptor)
        {
            //Test key import / Export
            SecuredVector<BYTE> datacopy(data.begin(), data.end());
            SecuredVector<BYTE> key;

            Test(cryptor.GetKey(key), "Symetric key export");

            auto other = CreateAnother(cryptor);
            Test(other->SetKey(key), "Key import");
            Test(cryptor.Encrypt(datacopy), "Encryption");
            Test(other->Decrypt(datacopy), "Decryption");
            Test(data.size() == datacopy.size() && memcmp(data.data(), datacopy.data(), data.size()) == 0, "Data validity");

            Test(other->Encrypt(datacopy), "Encryption");
            Test(cryptor.Decrypt(datacopy), "Decryption");
            Test(data.size() == datacopy.size() && memcmp(data.data(), datacopy.data(), data.size()) == 0, "Data validity");
        }

        unique_ptr<AsymetricEncryptor> CryptoTester::CreateAnother(const AsymetricEncryptor &object)
        {
            if (typeid(object) == typeid (RSAEncryptor))
            {
                return unique_ptr<RSAEncryptor>(new RSAEncryptor());
            }
            throw runtime_error("Failed to determine object type in CreateAnother");
        }

        unique_ptr<SymetricEncryptor> CryptoTester::CreateAnother(const SymetricEncryptor &object)
        {
            if (typeid(object) == typeid (AESEncryptor))
            {
                return unique_ptr<AESEncryptor>(new AESEncryptor());
            }
            else if (typeid(object) == typeid(RC4Encryptor))
            {
                return unique_ptr<RC4Encryptor>(new RC4Encryptor());

            }

            throw runtime_error("Failed to determine object type in CreateAnother");
        }

        void CryptoTester::EncodingTest()
        {
            EncodingTestForData(vector<BYTE>(smalldata, smalldata + strlen(reinterpret_cast<const char*>(smalldata))));
            EncodingTestForData(GenerateData(150));
            EncodingTestForData(GenerateData(1500));
            EncodingTestForData(GenerateData(150015));
        }

        void CryptoTester::EncodingTestForData(const vector<BYTE> &data)
        {
            string result = Encoding::EncodeToBase64(data);
            Test(!result.empty(), "Base 64 encoding");
            auto clear = Encoding::DecodeFromBase64(result);
            Test(clear == data, "Base 64 data bijection");

            result = Encoding::EncodeToHex(data);
            Test(!result.empty(), "hex encoding");
            clear = Encoding::DecodeFromHex(result);
            Test(clear == data, "Hex data bijection");

            result = Encoding::EncodeToHex(data, true);
            Test(!result.empty(), "hex encoding (caps)");
            clear = Encoding::DecodeFromHex(result);
            Test(clear == data, "Hex data bijection (caps)");
        }

        void CryptoTester::HashTest()
        {
            MD5 md5;
            vector<BYTE> data;
            data.resize(6);
            memcpy(data.data(), "foobar", 6);
            string result = md5.HashToHex(data);
            Test(result == "3858f62230ac3c915f300c664312c63f", "MD5 hash");

            SHA1 sha1;
            result = sha1.HashToHex(data);
            Test(result == "8843d7f92416211de9ebb963ff4ce28125932878", "SHA1 hash");

            SHA256 sha256;
            result = sha256.HashToHex(data);
            Test(result == "c3ab8ff13720e8ad9047dd39466b3c8974e592c2fa383d4a3960714caef0c4f2", "SHA256 hash");
            SHA256 other = std::move(sha256);
        }

        void CryptoTester::TestFactory()
        {
            Test(CryptoProvider::GetAsymetricEncryptionId(CryptoProvider::GetEncryptionTitle(AsymetricEncryption::RSA)) == AsymetricEncryption::RSA, "RSA factory");
            Test(CryptoProvider::GetSymetricEncryptionId(CryptoProvider::GetEncryptionTitle(SymetricEncryption::AES)) == SymetricEncryption::AES, "AES factory");
            Test(CryptoProvider::GetSymetricEncryptionId(CryptoProvider::GetEncryptionTitle(SymetricEncryption::RC4)) == SymetricEncryption::RC4, "RC4 factory");

            Test(CryptoProvider::GetHashFunctionId(CryptoProvider::GetHashFunctionTitle(HashFunction::MD5)) == HashFunction::MD5, "MD5 factory");
            Test(CryptoProvider::GetHashFunctionId(CryptoProvider::GetHashFunctionTitle(HashFunction::SHA1)) == HashFunction::SHA1, "SHA1 factory");
            Test(CryptoProvider::GetHashFunctionId(CryptoProvider::GetHashFunctionTitle(HashFunction::SHA256)) == HashFunction::SHA256, "SHA256 factory");
        }
    }
}