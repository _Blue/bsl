#include "Win32Tester.h"
#include "../../bsl/Win32/Win32Helper.h"
#include "TlHelp32.h"
#include "../../bsl/Win32/GUIHelper.h"
#include "resource.h"

using namespace std;
using namespace BSL::Win32;
namespace BSL
{
  namespace Test
  {
    void Win32Tester::Start()
    {
      //Compare GetProcessCommandLine with GetCommandLine
      string commandline;
      Test(Win32Helper::GetProcessCommandLine(OpenCurrentProcess(), commandline), "Get current process command line");
      Test(strcmp(commandline.data(), GetCommandLine()) == 0, "Command line validity");

      //Create registry key
          //Multibyte test
      CreateRegKey();
      Test(Win32Helper::RegistryDeleteTree(HKEY_CURRENT_USER, "Software\\BSL_Test"), "Reg key deletion");
      HKEY key;
      Test(RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\BSL_Test", 0, KEY_READ, &key) == ERROR_FILE_NOT_FOUND, "Key deletion success");

      //Unicode test
      CreateRegKey();
      Test(Win32Helper::RegistryDeleteTree(HKEY_CURRENT_USER, L"Software\\BSL_Test"), "Reg key deletion");
      Test(RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\BSL_Test", 0, KEY_READ, &key) == ERROR_FILE_NOT_FOUND, "Key deletion success");

      Test(!Win32Helper::GetCallStack().empty(), "Callstack validity");
      Test(!Win32Helper::GetExecutionDirectory().empty(), "Current directory validity");


      TestGUI();
    }

    HANDLE Win32Tester::OpenCurrentProcess()
    {
      return OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessId());
    }

    void Win32Tester::CreateRegKey()
    {
      HKEY key;
      Test(RegCreateKeyEx(HKEY_CURRENT_USER, "Software\\BSL_Test\\Key\\Subkey\\SubSubKey", 0, nullptr, 0, KEY_ALL_ACCESS, nullptr, &key, nullptr) == 0, "Key creation");
      Test(RegCloseKey(key) == 0, "Key close");
      Test(RegCreateKeyEx(HKEY_CURRENT_USER, "Software\\BSL_Test\\Key\\Subkey\\SubSubKey2", 0, nullptr, 0, KEY_ALL_ACCESS, nullptr, &key, nullptr) == 0, "Key creation");
      Test(RegCloseKey(key) == 0, "Key close");
    }

    void Win32Tester::TestGUI()
    {
      HWND window = CreateWindow(WC_BUTTON, "DoILikeFooORBar", 0, 0, 0, 42, 42, nullptr, nullptr, nullptr, nullptr);
      Test(window != 0, "Window creation");

      Test(GUIHelper::WindowTextA(window) == "DoILikeFooORBar", "Window text");
      
      Test(DestroyWindow(window), "Window destruction");

      window = CreateDummyWindow();

      uint resizeid;
      GUIHelper::AutoResizeWindow(window, resizeid);

      ShowWindow(window, SW_SHOW);

      MSG msg;
      while (GetMessageA(&msg, nullptr, 0, 0))
      {
        DispatchMessageA(&msg);
      }

      Test(DestroyWindow(window), "Window destruction");
    }

    HWND Win32Tester::CreateDummyWindow()
    {
      return CreateDialog(nullptr, MAKEINTRESOURCE(IDD_RESIZETEST), nullptr, Win32Tester::WNDProc);
    }

    ::INT_PTR Win32Tester::WNDProc(HWND window, ::UINT msg, WPARAM wparam, LPARAM lparam)
    {

      if (msg == WM_INITDIALOG)
      {
        return true;
      }
      else if (msg == WM_CLOSE)
      {
        EndDialog(window, 0);
      }
      return false;
    }
  }
}