#include <thread>
#include <algorithm>
#include "../../bsl/DataStructures/SecuredTCPMessageReader.h"
#include "../../bsl/DataStructures/SecuredTCPMessageWriter.h"
#include "SecuredTCPTester.h"
#define TCP_PORT 1080

using namespace BSL::Network;
using namespace BSL::DataStructures;
using namespace BSL::Crypto;
using namespace std;
namespace BSL
{
    namespace Test
    {
        void SecuredTCPTester::Start()
        {
            TestReadWrite();

            for (auto easym : CryptoProvider::GetSupportedAsymetricEncryptions())
            {
                for (auto esym : CryptoProvider::GetSupportedSymetricEncryptions())
                {
                    SecuredTCPOptions options;
                    options.AddAllowedEncryption(easym);
                    options.AddAllowedEncryption(esym);
                    CheckOptions(options);//Check without message signature first
                    for (auto hash : CryptoProvider::GetSupportedHashFunctions())
                    {
                        SecuredTCPOptions suboptions(options);
                        suboptions.AddAllowedHashFunction(hash);//Add hash function here
                        suboptions.SetSecret({ '1','3','3','Z' });
                        CheckOptions(suboptions);
                    }
                }
            }
        }

        void SecuredTCPTester::CheckOptions(const SecuredTCPOptions &options)
        {
            auto members = CreateConnection(options);
            TestExchange(*members.first, *members.second);
        }

        void SecuredTCPTester::TestReadWrite()
        {
            auto encryptions = CryptoProvider::GetSupportedSymetricEncryptions();
            auto hashfunctions = CryptoProvider::GetSupportedHashFunctions();
            SecuredVector<byte> secret = { 'b', 'a', 'r', 'i', 's', 'f', 'o', 'o' };
            for (auto ecrypt : encryptions)
            {
                for (auto ehash : hashfunctions)
                {
                    TestPacketEncoding(ecrypt, ehash, secret);
                }
                TestPacketEncoding(ecrypt, HashFunction::None, {});
            }
        }

        void SecuredTCPTester::TestPacketEncoding(SymetricEncryption asym, Crypto::HashFunction hashfunction, const SecuredVector<byte> &secret)
        {
            std::unique_ptr<SymetricEncryptor> cryptor;
            SymetricEncryptor *cryptorptr = nullptr;
            if (asym != SymetricEncryption::None)
            {
                cryptor = CryptoProvider::Construct(asym);
                cryptorptr = cryptor.get();
                Test(cryptorptr->GenerateKey(CryptoProvider::GetDefaultKeySize(asym)), "Key generation");
            }

            std::unique_ptr<HashBase> hash;
            HashBase *hashptr = nullptr;
            if (hashfunction != HashFunction::None)
            {
                hash = CryptoProvider::Construct(hashfunction);
                hashptr = hash.get();
            }

            SecuredVector<byte> data = { 'f', 'o', 'o' };
            SecuredTCPMessageWriter writer(data);
            auto encrypted = writer.GetData(cryptorptr, hashptr, secret);

            SecuredTCPMessageReader reader(encrypted);

            Test(reader.IsValid(hashptr, secret) == TCPError::Sucess, "SecuredTCP message valid signature");
            Test(reader.GetData(cryptorptr, hashptr) == data, "TCP message Read / Write");
        }

        pair<unique_ptr<SecuredTCPClient>, unique_ptr<SecuredTCPClient>> SecuredTCPTester::CreateConnection(const SecuredTCPOptions &options)
        {
            std::pair<unique_ptr<SecuredTCPClient>, unique_ptr<SecuredTCPClient>> ret;//init client and server

            ret.first = make_unique<SecuredTCPClient>(options);
            ret.second = make_unique<SecuredTCPClient>(options);
            std::thread thread(&SecuredTCPTester::StartServer, this, ret.second.get());//Start server connection on a new thread

            Test(ret.first->Connect("127.0.0.1", TCP_PORT) == TCPError::Sucess, "TCP connection");//Attempt client connection in this thread

            thread.join();//Wait for the end of the server connection thread
            return ret;
        }

        bool SecuredTCPTester::ClientConnected(const ClientInfos &infos)
        {
            _clientinfos = infos;
            return false;
        }

        void SecuredTCPTester::TestExchange(SecuredTCP &client, SecuredTCP &server)
        {
            Test(client.Send("foo") == TCPError::Sucess, "Secured TCP send");
            Test(server.ReceiveStr() == "foo", "Secured TCP receive");

            auto data = GenerateData(80000);
            Test(client.Send(data) == TCPError::Sucess, "SecuredTCP send");

            SecuredVector<byte> result;
            Test(server.Receive(result, 80000) == TCPError::Sucess, "SecuredTCP receive");

            Test(std::equal(result.begin(), result.end(), data.begin(), data.end()), "SecuredTCP data validity");
        }

        void SecuredTCPTester::StartServer(SecuredTCPClient *server)
        {
            TCPServer tcpserver;
            Test(tcpserver.Start(TCP_PORT, *this, "127.0.0.1") == TCPError::Sucess, "TCP server start");
            Test(server->Connect(_clientinfos.socket) == TCPError::Sucess, "SecuredTCP connection");
        }
    }
}