#pragma once

#include "Tester.h"
#include <Windows.h>
namespace BSL
{
  namespace Test
  {
    class Win32Tester : public Tester
    {
    public:
      Win32Tester() = default;

      void Start();

    private:

      static ::INT_PTR CALLBACK WNDProc(HWND window, ::UINT msg, WPARAM wparam, LPARAM lparam);
      void CreateRegKey();
      void TestGUI();
      HANDLE OpenCurrentProcess();
      HWND CreateDummyWindow();
    };
  }
}