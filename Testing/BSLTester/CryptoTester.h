#pragma once
#include "stdafx.h"
#include "Tester.h"
#include "../../bsl/Crypto/RSAEncryptor.h"
#include "../../bsl/Crypto/AESEncryptor.h"


namespace BSL
{
    namespace Test
    {
        class CryptoTester : protected Tester
        {
        public:
            CryptoTester();
            void Start();
            void TestClass(Crypto::AsymetricEncryptor &cryptor);
            void TestClass(Crypto::SymetricEncryptor &cryptor);

        private:
            void GeneralTest(const SecuredVector<BYTE> &data, Crypto::CryptoBase &cryptor);
            void AsymetricTest(const SecuredVector<BYTE> &data, Crypto::AsymetricEncryptor &cryptor);
            void SymetricTest(const SecuredVector<BYTE> &data, Crypto::SymetricEncryptor &cryptor);
            void EncodingTest();
            void EncodingTestForData(const std::vector<BYTE> &data);
            void HashTest();
            void TestFactory();
            
            std::unique_ptr<Crypto::AsymetricEncryptor> CreateAnother(const Crypto::AsymetricEncryptor &object);
            std::unique_ptr<Crypto::SymetricEncryptor> CreateAnother(const Crypto::SymetricEncryptor &object);

        };
    }
}