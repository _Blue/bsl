#The Blue Software Library
The bsl is a C++ library containing various data structures, wrapper and tools to develop a project faster  
Features:  

* Abstract layer for the WinCrypt API  
* Wrapper for secured TCP communications  
* String processing functions  
* IO redirection functions  
* Threaded pattern matching functions

##Development status
Version 3.0 is in development, as previous version were private, the bsl can not be downloaded yet.  
Note that the master branch will only contain stable release, as there is no stable release yet, the master branch is empty.  
Dev builds are done in the branch "dev".

##Build
To build the bsl, you need Microsoft Visual Studio.  
To build with the GUI, just compile in Visual Studio  
To build in a command prompt, just run msbuild bsl.sln  

##Linking
Both MT, MD, MTd and MDd targets are available.  
The bsl also supports X86 and X64 compilation.

##Dependencies
The bsl depends on the following libraries (included in Visual Studio environement): 

* WS2_32.lib  
* psapi.lib  
* ole32.lib  
* uuid.lib  

These libraries are automatically linked using "#pragma" system, so you do not need to add them manually

##Project configuration
To add the bsl to your project, open linker settings, add the bsl binary folder to the linked library settings. 
Depending on your compilation targets, the library files will be linked automatically  
Also add the BSL's header to your header search paths.

##Copyright
You are allowed to use, modify and redistribute the BSL within any project, open source or not.  

##More infos
[website](http://bluecode.fr)
