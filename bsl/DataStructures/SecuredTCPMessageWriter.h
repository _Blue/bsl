/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: SecuredTCPMessageWriter.h
//Purpose: Contains class used to Write Secured TCP Messages
#pragma once
#include "SecuredVector.h"
namespace BSL
{
    namespace Crypto
    {
        class SymetricEncryptor;
        class AsymetricEncryptor;
        class HashBase;
    }

    namespace DataStructures
    {
        class SecuredTCPMessageWriter
        {
        public:
            SecuredTCPMessageWriter(SecuredVector<byte> data);

            SecuredVector<byte> GetData(Crypto::SymetricEncryptor *encryptor, Crypto::HashBase *hashfunction, const SecuredVector<byte> &sharesecret);//Returns encrypted message

        private:
            SecuredVector<byte> _data;
        };
    }
}