/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: SecuredVector.h
//Purpose: Simple subclass of vector 
//The point is to zeroise data when the vector is destroyed
//Avoiding sensible data to stick in memory

#pragma once
#include "SecuredAllocator.h"
#include "../Crypto/Encoding.h"
#include "../Tools/Exception.h"
namespace BSL//Because this data structure in being used in all the library, it is preferable to have it in the root namespace
{

    enum class DataFormat : byte
    {
        Raw,//Treat the string as a raw input, does not include the '\0'
        Base64,
        Hex,
        HexCaps,//Same as Hex but letters are capitals
    };
    template <class T>
    class SecuredVector : public std::vector<T, SecuredAllocator<T>>
    {
    public:
        using std::vector<T, SecuredAllocator<T>>::vector;
        SecuredVector() : vector<T, SecuredAllocator<T>>()//Default ctor, simply call base ctor
        {

        }

        SecuredVector(const std::vector<T> &other) : vector<T, SecuredAllocator<T>>(other.begin(), other.end())//Copy ctor from vector
        {

        }

        SecuredVector(std::vector<T, SecuredAllocator<T>> &&other) : vector<T, SecuredAllocator<T>>(other)//Move ctor from vector
        {

        }

        SecuredVector(const std::string &data, DataFormat format) : vector<T, SecuredAllocator<T>>(ConvertData(format))//Constructor from a string, format must be provided, throws in case of invalid format
        {
            //First build an empty vector with the default base contructor
            if (!ConvertData(data, format))//Then convert data
            {
                throw Exception("Invalid format");
            }
        }

#ifdef BSL_ALLOW_UNSAFE_CONVERSIONS
        operator std::vector<T>() const
        {
            return std::vector<T>(begin(), end());//Will create a copy of the vector that is not being to be zeroised when destroyed
        }
#endif

        //Conversion functions

        std::string ToBase64() const
        {
            return Crypto::Encoding::EncodeToBase64(reinterpret_cast<const byte *>(data()), size() * sizeof(T));
        }

        std::string ToHex(bool caps) const
        {
            return Crypto::Encoding::EncodeToHex(reinterpret_cast<const byte *>(data()), size() * sizeof(T), caps);
        }

    private:
        bool ConvertData(std::string &data, DataFormat format)
        {
            if (data.empty())
            {
                return true;//Nothing to do in this case
            }

            if (format == DataFormat::Raw)
            {
                insert(begin(), data.begin(), data.end());
                return true;
            }

            std::vector<byte> res;
            else if (format == DataFormat::Base64)
            {
                res = Crypto::Encoding::DecodeFromBase64(data);
                insert(begin(), res.begin(), res.end());
                return true;
            }
            else if (format == DataFormat::Hex || format == DataFormat::HexCaps)
            {
                res = Crypto::Encoding::DecodeFromBase64(data);

                insert(begin(), res.begin(), res.end());
                return true;
            }

            return res.empty();
        }
    };
}