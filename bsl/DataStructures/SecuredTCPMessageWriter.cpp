/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


/*This file provides an abstraction layer to write messages used in SecuredTCP interface
The message format is:
[UINT32] message size, the whole message size (including the signature and this value)
[Hash] The message signature, size depends of the hash function, its value is hash(message + shared_secret)
[Data] the actual message, its size is: message_size - sizeof(UINT32) - hash_size
*/
#include "SecuredTCPMessageWriter.h"
#include "../Crypto/HashBase.h"
#include "../Crypto/SymetricEncryptor.h"
#include "../Tools/Exception.h"
using namespace std;
using namespace BSL::Crypto;
namespace BSL
{
    namespace DataStructures
    {
        SecuredTCPMessageWriter::SecuredTCPMessageWriter(SecuredVector<byte> data) :
            _data(data)
        {

        }

        SecuredVector<byte> SecuredTCPMessageWriter::GetData(SymetricEncryptor *encryptor, HashBase *hashfunction, const SecuredVector<byte> &secret)
        {
            size_t hashsize = 0;
            if (hashfunction != nullptr)
            {
                hashsize = hashfunction->GetHashLen();
            }

            std::vector<byte> ret(sizeof(uint));//Add only the header size

            if (encryptor != nullptr && !encryptor->Encrypt(_data))
            {
                throw Exception("Encryption error");
            }

            ret.insert(ret.end(), _data.begin(), _data.end());//Add the encrypted blob the vector

            if (hashfunction != nullptr)
            {
                SecuredVector<byte> hashdata;//Prepare a vector to hold the data to be hashed
                hashdata.insert(hashdata.begin(), _data.begin(), _data.end());
                hashdata.insert(hashdata.end(), secret.begin(), secret.end());

                vector<byte> hashvalue;

                if (!hashfunction->Hash(hashdata.data(), hashdata.size(), hashvalue))//Compute the message's signature
                {
                    throw Exception("Hash error");
                }
                ret.insert(ret.begin() + sizeof(uint), hashvalue.begin(), hashvalue.end());//Add the signature to the message
            }

            *reinterpret_cast<uint*>(ret.data()) = ret.size();//Finally add the size header
            return ret;
        }
    }
}