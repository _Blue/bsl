/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: SecuredAllocator.h
//Purpose: Provide normal allocator and zeroization deallocator
#pragma once

#include <allocators>
#include <string.h>

namespace BSL
{
    template <class T>

    class SecuredAllocator : public std::allocator<T>
    {
    public:
        typedef T          value_type;
        typedef size_t     size_type;
        typedef ptrdiff_t  difference_type;
        typedef T*         pointer;
        typedef const T*   const_pointer;
        typedef T&         reference;
        typedef const T&   const_reference;

        SecuredAllocator(int n) : std::allocator<T>(n)
        {
        }

        SecuredAllocator() : std::allocator<T>()
        {
        }

        SecuredAllocator(const SecuredAllocator &other) : std::allocator<T>(other)
        {

        }

        template <class Other>
        SecuredAllocator(const SecuredAllocator<Other> &other) : std::allocator<T>(other)
        {

        }

        template <class U>
        struct rebind
        {
            typedef SecuredAllocator<U> other;
        };

        T *allocate(size_type size, const void* hint = nullptr)
        {
            return std::allocator<T>::allocate(size, hint);
        }

        void deallocate(T *address, size_type length)
        {
            memset(address, 0, length * sizeof(T));//Zeroise memory
            std::allocator<T>::deallocate(address, length);
        }
    };

    template <class T>
    bool operator==(const SecuredAllocator<T>& left, const SecuredAllocator<T>& right)
    {
        return static_cast<std::allocator<T>>(left) == static_cast<std::allocator<T>>(right);
    }

    template <class T>
    bool operator!=(const SecuredAllocator<T>& left, const SecuredAllocator<T>& right)
    {
        return static_cast<std::allocator<T>>(left) != static_cast<std::allocator<T>>(right);
    }
}