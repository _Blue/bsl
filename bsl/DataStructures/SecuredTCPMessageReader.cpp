/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


/*This file provides an abstraction layer to read messages used in SecuredTCP interface
The message format is:
[UINT32] message size, the whole message size (including the signature and this value)
[Hash] The message signature, size depends of the hash function, its value is hash(message + shared_secret)
[Data] the actual message, its size is: message_size - sizeof(UINT32) - hash_size
*/
#include <algorithm>
#include "SecuredTCPMessageReader.h"
#include "../Crypto/HashBase.h"
#include "../Crypto/SymetricEncryptor.h"
#include "../Tools/Exception.h"
#include "../Network/TCpError.h"
using namespace std;
using namespace BSL::Crypto;
using namespace BSL::Network;
namespace BSL
{
    namespace DataStructures
    {
        SecuredTCPMessageReader::SecuredTCPMessageReader(const SecuredVector<byte> &data) :
            _data(data)
        {

        }

        TCPError SecuredTCPMessageReader::IsValid(HashBase *hash, const SecuredVector<byte> &secret)
        {
            size_t hashlen = 0;
            if (hash != nullptr)
            {
                hashlen = hash->GetHashLen();
            }
            //First check the message size
            if (_data.size() <= sizeof(uint) + hashlen)
            {
                return TCPError::EProtocolMismatch;
            }

            //Then verify that the message size matches the first UINT32 of the message
            uint sizeheader = *reinterpret_cast<const uint*>(_data.data());
            if (sizeheader != _data.size())
            {
                return TCPError::EProtocolMismatch;
            }

            if (hash == nullptr)
            {
                return TCPError::Sucess;
            }

            //Now that we know the size is valid, verify the signature
            SecuredVector<byte> hashdata(_data.data() + sizeof(uint) + hashlen, _data.data() + _data.size());//Create a vector to hold the data to be hashed
            hashdata.insert(hashdata.end(), secret.begin(), secret.end());//Append secret

            vector<BYTE>  hashvalue;
            if (!hash->Hash(hashdata, hashvalue))
            {
                return TCPError::EEncryptionError;
            }

            if (!std::equal(_data.cbegin() + sizeof(uint), _data.cbegin() + sizeof(uint) + hashlen, hashvalue.begin(), hashvalue.end()))
            {
                return TCPError::EInvalidSignature;
            }
            return TCPError::Sucess;
        }

        SecuredVector<BYTE> SecuredTCPMessageReader::GetData(SymetricEncryptor *encryptor, HashBase *hash)
        {
            size_t hashlen = 0;
            if (hash != nullptr)
            {
                hashlen = hash->GetHashLen();
            }
#ifdef _DEBUG
            if (_data.size() <= sizeof(uint) + hashlen)//Just in case
            {
                throw BSL::Exception("Invalid SecuredTCPMessage in SecuredTCPMessageReader::GetData");
            }
#endif

            SecuredVector<byte> result(_data.cbegin() + sizeof(uint) + hashlen, _data.cend());
            if (encryptor != nullptr && !encryptor->Decrypt(result))
            {
                throw BSL::Exception("Decryption error");
            }
            return result;
        }

    }
}