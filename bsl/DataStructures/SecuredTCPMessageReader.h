/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: SecuredTCPMessageReader.h
//Purpose: Contains a class used to read SecuredTCP messages
#pragma once
#include "SecuredVector.h"
namespace BSL
{
    namespace Crypto
    {
        class SymetricEncryptor;
        class AsymetricEncryptor;
        class HashBase;
    }

    namespace Network
    {
        class TCPError ;
    }

    namespace DataStructures
    {
        class SecuredTCPMessageReader
        {
        public:
            SecuredTCPMessageReader(const SecuredVector<byte> &data);//Ctor that copies data
            
            SecuredVector<byte> GetData(Crypto::SymetricEncryptor *encryptor, Crypto::HashBase *hashfunction);//Returns decrypted data
            Network::TCPError IsValid(Crypto::HashBase *hashfunction, const SecuredVector<byte> &secret);//Returns true if the message signature is valid and the signature is valid

        private:
            const SecuredVector<byte> &_data;
        };
    }
}