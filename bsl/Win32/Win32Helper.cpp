/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include <sstream>
#include <vector>
#include "Win32Helper.h"
#include "../Tools./Exception.h"
using namespace std;

namespace BSL
{
    namespace Win32
    {
        bool Win32Helper::RegistryDeleteTree(HKEY key, const char *name)
        {
            /*
            This function is intended for pre-vista Windows, which don't have RegDeletree function
            To be able to delete simply registry keys, we do a depthfirst traversal
            deleting all sub keys before deleting the high level key
            */
            DWORD maxname;
            DWORD namesize;
            HKEY subkey;
            if (RegOpenKeyExA(key, name, 0, KEY_ALL_ACCESS, &subkey))
            {
                return false;
            }

            //Compute the maximum subkey size (to allocate our buffer correctly)
            if (RegQueryInfoKeyA(subkey, nullptr, nullptr, nullptr, nullptr, &maxname, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr))
            {
                return false;
            }


            //Delete the sub-keys one by one 
            uint ret = 0;
            string keyname;
            maxname++;
            keyname.resize(maxname);
            while (ret == 0)
            {
                namesize = maxname + 1;
                ret = RegEnumKeyExA(subkey, 0, const_cast<char*>(keyname.data()), &namesize, nullptr, nullptr, nullptr, nullptr);
                if (ret == ERROR_SUCCESS)
                {
                    if (!RegistryDeleteTree(subkey, keyname.data()))
                    {
                        RegCloseKey(subkey);
                        return false;
                    }
                }
                else if (ret != ERROR_NO_MORE_ITEMS)
                {
                    break;
                }
            }
            RegCloseKey(subkey);

            //Finally remove the key when it has no more children
            return ret == ERROR_NO_MORE_ITEMS && RegDeleteKeyA(key, name) == NO_ERROR;
        }

        bool Win32Helper::RegistryDeleteTree(HKEY key, const wchar_t *name)
        {
            DWORD maxname;
            DWORD namesize;
            HKEY subkey;
            if (RegOpenKeyExW(key, name, 0, KEY_ALL_ACCESS, &subkey))
            {
                return false;
            }

            if (RegQueryInfoKeyW(subkey, nullptr, nullptr, nullptr, nullptr, &maxname, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr))
            {
                return false;
            }

            uint ret = 0;
            wstring keyname;
            maxname++;
            keyname.resize(maxname);
            while (ret == 0)
            {
                namesize = maxname + 1;
                ret = RegEnumKeyExW(subkey, 0, const_cast<wchar_t*>(keyname.data()), &namesize, nullptr, nullptr, nullptr, nullptr);
                if (ret == ERROR_SUCCESS)
                {
                    if (!RegistryDeleteTree(subkey, keyname.data()))
                    {
                        RegCloseKey(subkey);
                        return false;
                    }
                }
                else if (ret != ERROR_NO_MORE_ITEMS)
                {
                    break;
                }
            }
            RegCloseKey(subkey);
            return ret == ERROR_NO_MORE_ITEMS && RegDeleteKeyW(key, name) == NO_ERROR;
        }

        void Win32Helper::CreateDynamicConsole()
        {
            AllocConsole();
            freopen("CONIN$", "r", stdin);
            freopen("CONOUT$", "w", stdout);
            freopen("CONOUT$", "w", stderr);
            ios_base::sync_with_stdio();
        }

        bool Win32Helper::GetProcessCommandLine(HANDLE process, string &commandline)
        {
            /*
            This function is a bit tricky. Because the WIN32 API does not provide an "easy" way
            to retrieve a process' command line, we have to read the process control block to retrive
            the command line

            To do this, we call NtQueryInformationProcess to get the command line's address
            and then we read it using ReadProcessMemory
            */

            //Struct declaration
            struct PROCESS_BASIC_INFORMATION
            {
                NTSTATUS ExitStatus;
                PVOID PebBaseAddress;
                ULONG_PTR AffinityMask;
                DWORD BasePriority;
                HANDLE UniqueProcessId;
                HANDLE InheritedFromUniqueProcessId;
            };

            struct PEB_LDR_DATA
            {
                BYTE       Reserved1[8];
                PVOID      Reserved2[3];
                LIST_ENTRY InMemoryOrderModuleList;
            };

            struct UNICODE_STRING
            {
                USHORT Length;
                USHORT MaximumLength;
                PWSTR  Buffer;
            };

            struct RTL_USER_PROCESS_PARAMETERS
            {
                BYTE           Reserved1[16];
                PVOID          Reserved2[10];
                UNICODE_STRING ImagePathName;
                UNICODE_STRING CommandLine;
            };

            struct PEB
            {
                BYTE                          Reserved1[2];
                BYTE                          BeingDebugged;
                BYTE                          Reserved2[1];
                PVOID                         Reserved3[2];
                PEB_LDR_DATA                 *Ldr;
                RTL_USER_PROCESS_PARAMETERS  *ProcessParameters;
                BYTE                          Reserved4[104];
                PVOID                         Reserved5[52];
                LPVOID						  PostProcessInitRoutine;
                BYTE                          Reserved6[128];
                PVOID                         Reserved7[1];
                ULONG                         SessionId;
            };

            typedef NTSTATUS(NTAPI *_NtQueryInformationProcess)(HANDLE ProcessHandle, DWORD ProcessInformationClass, PVOID ProcessInformation, DWORD ProcessInformationLength, PDWORD ReturnLength);

            //Load NtQueryInformationProcess from its dll
            _NtQueryInformationProcess NtQueryInformationProcess = reinterpret_cast<_NtQueryInformationProcess>(GetProcAddress(GetModuleHandle("ntdll.dll"), "NtQueryInformationProcess"));
            if (NtQueryInformationProcess == nullptr)
            {
                return false;
            }


            //Retrieve the process control block
            PROCESS_BASIC_INFORMATION pinfo;
            LONG status = NtQueryInformationProcess(process, 0, &pinfo, sizeof(PVOID) * 6, nullptr);
            if (status)
            {
                return false;
            }
            PEB *peb = reinterpret_cast<PEB*>(reinterpret_cast<void **>(&pinfo)[1]);

            PEB ppebCopy;
            if (!ReadProcessMemory(process, peb, &ppebCopy, sizeof(PEB), nullptr))
            {
                return false;
            }

            RTL_USER_PROCESS_PARAMETERS rtlparams;
            if (!ReadProcessMemory(process, ppebCopy.ProcessParameters, &rtlparams, sizeof(RTL_USER_PROCESS_PARAMETERS), nullptr))
            {
                return false;
            }

            //Get the command line length
            ushort len = rtlparams.CommandLine.Length / sizeof(wchar_t);

            //Copy the buffer of the proces in our memory
            wstring ret;
            ret.resize(len + 1);
            if (!ReadProcessMemory(process, rtlparams.CommandLine.Buffer, const_cast<wchar_t*>(ret.data()), len * sizeof(WCHAR), nullptr))
            {
                return false;
            }

            commandline = string(ret.begin(), ret.end());
            return true;
        }

        string Win32Helper::GetExecutionDirectory()
        {
            //Call the function with a zero len to retrieve the string length
            DWORD len = GetCurrentDirectory(0, nullptr);

            string ret;
            ret.resize(len - 1);//len includes the '\0'
            if (GetCurrentDirectory(len, const_cast<char*>(ret.data())) != len - 1)//Because if buffer is non zero, the return includes the '\0'
            {
                throw Exception("Unexpected buffer size returned by GetCurrentDirectory");//Should never happen, but better check
            }

            return ret;
        }

        string Win32Helper::GetCallStack(ushort frames)
        {
            //Load library
            HMODULE hlib = LoadLibrary("Dbghelp.dll");
            if (hlib == nullptr)
            {
                return "";
            }


            //Declare structure
            struct SYMBOL_INFO
            {
                ULONG       SizeOfStruct;
                ULONG       TypeIndex;
                ULONG64     Reserved[2];
                ULONG       Index;
                ULONG       Size;
                ULONG64     ModBase;
                ULONG       Flags;
                ULONG64     Value;
                ULONG64     Address;
                ULONG       Register;
                ULONG       Scope;
                ULONG       Tag;
                ULONG       NameLen;
                ULONG       MaxNameLen;
                CHAR        Name[MAX_PATH];
            };

            //Load functions
            typedef BOOL(__stdcall *SymInitializetype)(HANDLE, PCSTR, BOOL);
            typedef BOOL(__stdcall *SymFromAddrtype)(HANDLE, DWORD64, PDWORD64, SYMBOL_INFO*);
            SymInitializetype SymInitialize = reinterpret_cast<SymInitializetype>(GetProcAddress(hlib, "SymInitialize"));
            if (SymInitialize == nullptr)
            {
                FreeLibrary(hlib);
                return "";
            }
            SymFromAddrtype SymFromAddr = reinterpret_cast<SymFromAddrtype>(GetProcAddress(hlib, "SymFromAddr"));
            if (SymFromAddr == nullptr)
            {
                FreeLibrary(hlib);
                return nullptr;
            }

            
            //Init structures
            vector<void *> stack(frames);
            SYMBOL_INFO symbol;
            ZeroMemory(&symbol, sizeof(SYMBOL_INFO));
            symbol.SizeOfStruct = sizeof(SYMBOL_INFO) - MAX_PATH + sizeof(size_t);
            symbol.MaxNameLen = MAX_PATH;


            //Call SymInitialize with our process
            HANDLE process = GetCurrentProcess();
            if (!SymInitialize(process, nullptr, true))
            {
                FreeLibrary(hlib);
                return nullptr;
            }

            //Capture frames and iterate to get all functions names and addresses
            stringstream ret;
            frames = CaptureStackBackTrace(0, frames, stack.data(), nullptr);
            uint i;
            for (i = 0; i < frames; i++)
            {
                if (!SymFromAddr(process, reinterpret_cast<DWORD64>(stack[i]), 0, &symbol))
                {
                    break;
                }
                if (i != 0)
                {
                    ret << endl;
                }

                ret << symbol.Name << ", at: " << std::hex << symbol.Address;
            }
            FreeLibrary(hlib);

            if (i != frames)//In case an error occured
            {
                return "";
            }
            return ret.str();
        }
    }
}