/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: Win32Helper.h
//Purpose: Shortcuts for common Win32 functions and API's

/*
Fun fact:
Some functions names had to be renamed because some MS splendid macros, including: 
-RegDeleteTree
-GetCurrentDirectory
*/

#pragma once
#include <windows.h>
#include <string>
#include "../BSLDefs.h"
namespace BSL
{
    namespace Win32
    {
        class Win32Helper
        {
        public:
            //Removes the specified key, its values and all its subkeys, returns  false on error
            static bool RegistryDeleteTree(HKEY key, const char *name);
            static bool RegistryDeleteTree(HKEY key, const wchar_t *name);

            //Creates a dynamic console, useful when compiling in /SUBSYSTEM:Windows
            static void CreateDynamicConsole();

            //Returns the command line of the specified process, fails if the process' architecture does not match ours
            static bool GetProcessCommandLine(HANDLE process, std::string &commandline);

            //Returns the execution directory of the process
            static std::string GetExecutionDirectory();

            //Returns a readable callstack, useful for debugging
            static std::string GetCallStack(ushort frames = _maxframes);

        private:
            static constexpr ushort _maxframes = 62;//Due to Windows XP limitations
        };
    }
}