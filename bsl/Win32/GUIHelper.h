/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: GUIHelper.h
//Purpose: Quick access function for the Win32 GUI system

#pragma once

#pragma comment(lib, "Comctl32.lib")//For SetWindowSubclass

#include "../BSLDefs.h"
#include <string>
#include <Windows.h>
#include <CommCtrl.h>
#include <vector>
#include <atomic>

namespace BSL
{
  namespace Win32
  {
    namespace Internal
    {
      struct WindowResizeData;
    }

    class GUIHelper
    {
    public:
      //Returns the content of an item in a ListView, returns empty string on error
      static std::string ListView_GetItemTextA(HWND listview, uint row, uint column);
      static std::wstring ListView_GetItemTextW(HWND listview, uint row, uint column);

      //Returns the text of the specified HWND, returns empty string on error
      static std::string WindowTextA(HWND window);
      static std::wstring WindowTextW(HWND window);

      //Apply the system font to all the child of the specified window, return false on error
      static bool ApplySystemFont(HWND window, const std::vector<std::wstring> &classes = { WC_BUTTONW, WC_EDITW, WC_TABCONTROLW });

      //Automatically resizes the controls of a window when its size changes, returns false on error
      //These functions MUST be called on the thread that created the window
      static bool AutoResizeWindow(HWND window, uint &resizeid);
      static bool StopWindowResize(HWND window, uint resizeid);

    private:
      static LRESULT WINAPI ResizeCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam, UINT_PTR id, DWORD_PTR param);
      static DWORD_PTR InitWindowResizeData(HWND window);
      static bool AddWindowData(HWND window, std::vector<Internal::WindowResizeData> &data, uint borderx, uint bordery);
      static bool ResizeWindow(Internal::WindowResizeData *data);
      static std::atomic<uint> _subclassid;
    };
  }
}