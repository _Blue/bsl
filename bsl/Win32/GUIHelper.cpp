/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "GUIHelper.h"
#include "../Tools/Exception.h"
#define BEGIN_SUBCLASSID 135
#define WM_BSL_STOPRESIZE WM_USER + BEGIN_SUBCLASSID
/*
For almost each function, this file contains an multibyte and an unicode implementation
We could imagine redirecting multibyte call to unicode call and converting parameter and return values
But doing this would imply lots of copy and calls to mbstowcs and wcstombs

For optimisation reasons, we prefer to implement each function twice, avoiding parameter
and results copies

Of course, template specialization could be an option, I'll think about it
*/

using namespace std;
using BSL::Win32::Internal::WindowResizeData;

namespace BSL
{
  namespace Win32
  {
    std::atomic<uint> GUIHelper::_subclassid = BEGIN_SUBCLASSID;

    struct Internal::WindowResizeData
    {
      HWND hwnd;
      size_t width;
      size_t height;
      size_t x;
      size_t y;
      float xratio = 1;//These are used to avoid useless redraws
      float yratio = 1;
      std::vector<WindowResizeData> controls;
    };

    string GUIHelper::ListView_GetItemTextA(HWND listview, uint row, uint column)
    {
      string ret;
      LVITEMA item;
      ZeroMemory(&item, sizeof(LVITEMA));
      item.iItem = row;
      item.iSubItem = column;
      item.cchTextMax = 32;
      ushort size = 16;
      LRESULT error;
      item.pszText = const_cast<char*>(ret.data());
      do
      {
        size *= 2;
        item.cchTextMax = size;
        ret.resize(size);
        error = SendMessageA(listview, LVM_GETITEMTEXTA, row, reinterpret_cast<LPARAM>(&item));
        if (item.cchTextMax <= 0 || error < 0)
        {
          throw Exception("Failed to retrieve content of a list view item");
        }
      } while (error >= size - 1);

      return ret;
    }

    wstring GUIHelper::ListView_GetItemTextW(HWND listview, uint row, uint column)
    {
      wstring ret;
      LVITEMW item;
      ZeroMemory(&item, sizeof(LVITEMW));
      item.iItem = row;
      item.iSubItem = column;
      item.cchTextMax = 32;
      ushort size = 16;
      LRESULT error;
      item.pszText = const_cast<wchar_t*>(ret.data());
      do
      {
        size *= 2;
        item.cchTextMax = size;
        ret.resize(size);
        error = SendMessageW(listview, LVM_GETITEMTEXTW, row, reinterpret_cast<LPARAM>(&item));
        if (item.cchTextMax <= 0 || error < 0)
        {
          throw Exception("Failed to retrieve content of a list view item");
        }
      } while (error >= size - 1);

      return ret;
    }

    string GUIHelper::WindowTextA(HWND window)
    {
      //Get the buffer length
      int size = GetWindowTextLengthA(window);
      if (size < 0)
      {
        throw Exception("Invalid window specified to BSL::GUIHelper::WindowTextA");
      }
      string ret;
      ret.resize(size);
      size++;//Include the '\0'
      GetWindowTextA(window, const_cast<char*>(ret.data()), size);
      return ret;
    }

    wstring GUIHelper::WindowTextW(HWND window)
    {
      //Get the buffer length
      int size = GetWindowTextLengthA(window);
      if (size < 0)
      {
        throw Exception("Invalid window specified to BSL::GUIHelper::WindowTextW");
      }
      size++;//Include the '\0'
      wstring ret;
      ret.resize(size);
      GetWindowTextW(window, const_cast<wchar_t*>(ret.data()), size);
      return ret;
    }

    bool GUIHelper::ApplySystemFont(HWND window, const vector<wstring> &classes)
    {
      //Retrieve the system default's font
      LOGFONTW lf;
      if (!GetObjectW(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONTW), &lf))
      {
        return false;
      }

      //Create a font matching the parameters
      HFONT font = CreateFontW(lf.lfHeight, lf.lfWidth, lf.lfEscapement, lf.lfOrientation, lf.lfWeight, lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet, lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality, lf.lfPitchAndFamily, lf.lfFaceName);
      if (font == nullptr)
      {
        return false;
      }

      wchar_t classname[257];//256 is the maximum class name (https://msdn.microsoft.com/en-us/library/windows/desktop/ms633582%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396)

      //Enumerate the childs of the window
      HWND child = GetWindow(window, GW_CHILD);
      while (child != nullptr)
      {
        //Get the class name
        if (GetClassNameW(child, classname, 256))
        {
          //Check that the class name matches one of our class
          for (const auto &e : classes)
          {
            if (wcscmp(classname, e.data()) == 0)
            {
              SendMessage(child, WM_SETFONT, reinterpret_cast<WPARAM>(font), true);//Apply the window font
              break;
            }
          }
        }
        child = GetWindow(child, GW_HWNDNEXT);
      }
      return true;
    }

    bool GUIHelper::AutoResizeWindow(HWND window, uint &resizeid)
    {
      /*
      In this function, we install a hook to get the WM_SIZE event and
      handle the event ourselves, to do this, we use SetWindowSubclass
      to force the SendMessage function to call our callback, ResizeCallback
      */

      //Generate a subclass id for this subclass
      resizeid = _subclassid++;

      //Create the window resize data
      DWORD_PTR data = InitWindowResizeData(window);

      //Install the hook
      return SetWindowSubclass(window, GUIHelper::ResizeCallback, resizeid, data);
    }

    bool GUIHelper::StopWindowResize(HWND window, uint resizeid)
    {
      //First call the WND proc with a message to signal that the hook is being removed
      SendMessage(window, WM_BSL_STOPRESIZE, 0, 0);

      //Then remove the subclass
      return RemoveWindowSubclass(window, GUIHelper::ResizeCallback, resizeid);
    }

    LRESULT WINAPI GUIHelper::ResizeCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam, UINT_PTR id, DWORD_PTR param)
    {
      /*
      This function has to handle two events:
       -When the window is resized
       -When the callback is uninstalled
      */

      if (message == WM_BSL_STOPRESIZE)
      {
        delete reinterpret_cast<Internal::WindowResizeData*>(param);
        return true;
      }
      else if (message == WM_SIZING)
      {
        if (ResizeWindow(reinterpret_cast<Internal::WindowResizeData*>(param)))
        {
          RedrawWindow(window, nullptr, nullptr, RDW_INVALIDATE);
        }

        //ResizeWindow can fail, but what should we do in this case ?
        //We have no way to return an error from here
        //And throwing an exception it certainly a bad idea
        //TODO: handle errors at this point

      }

      return DefSubclassProc(window, message, wparam, lparam);//Call the original function
    }

    DWORD_PTR GUIHelper::InitWindowResizeData(HWND window)
    {
      WindowResizeData *data = new WindowResizeData();
      data->hwnd = window;

      RECT rect;
      if (!GetClientRect(window, &rect))
      {
        throw BSL::Exception("Failed to obtain the size of the window in GUIHelper::InitWindowResizeData");
      }

      //Compute the size of window borders

      RECT adjustedrect = rect;

      LONG style = GetWindowLongPtr(window, GWL_STYLE);
      LONG exstyle = GetWindowLongPtr(window, GWL_EXSTYLE);
      bool menu = GetMenu(window) != nullptr;

      if (!AdjustWindowRectEx(&adjustedrect, style, menu, exstyle))
      {
        throw BSL::Exception("Failed to obtain the size of the window in GUIHelper::InitWindowResizeData");
      }

      uint xborder = std::abs(adjustedrect.left - rect.left);
      uint yborder = std::abs(adjustedrect.top - rect.top);

      //Save the window size in the structure
      data->width = rect.right - rect.left;
      data->height = rect.bottom - rect.top;

      //Get first child of the window
      window = GetWindow(window, GW_CHILD);

      //iterate through controls
      while (window != nullptr)
      {
        AddWindowData(window, data->controls, xborder, yborder);
        window = GetWindow(window, GW_HWNDNEXT);
      }

      return reinterpret_cast<DWORD_PTR>(data);
    }

    bool GUIHelper::AddWindowData(HWND window, std::vector<WindowResizeData> &vect, uint borderx, uint bordery)
    {
      WindowResizeData data;

      RECT rect;
      if (!GetWindowRect(window, &rect))
      {
        return false;
      }

      data.hwnd = window;
      data.x = rect.left - borderx;
      data.y = rect.top - bordery;
      data.width = rect.right - rect.left;
      data.height = rect.bottom - rect.top;

      vect.push_back(data);
      return true;
    }

    bool BSL::Win32::GUIHelper::ResizeWindow(WindowResizeData *data)
    {
      //This function applies the new size of the controls of a window
      //To do this, we get the new size of the window
      //The we compute a "change ratio" and we apply it to the controls

      RECT rect;
      if (!GetClientRect(data->hwnd, &rect))
      {
        return false;
      }

      float xratio = static_cast<float>(rect.right - rect.left);
      xratio /= data->width;

      float yratio = rect.bottom - rect.top;
      yratio /= static_cast<float>(data->height);


      //check that the operation is not useless (in case the ratio did not change)
      if (xratio == data->xratio && yratio == data->yratio)
      {
        return false;
      }
      
      data->xratio = xratio;
      data->yratio = yratio;

      //Then we apply the ration the the sub controls
      for (auto const &e : data->controls)
      {
        //We use HWND_TOP, because if the window is being resized, it means the window has focus
        MoveWindow(e.hwnd, e.x * xratio, e.y * yratio, e.width * xratio, e.height * yratio, false);
      }

      return true;
    }
  }
}