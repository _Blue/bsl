/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#pragma once
#include "CryptoProvider.h"
#include "RSAEncryptor.h"
#include "AESEncryptor.h"
#include "RC4Encryptor.h"
#include "MD5.h"
#include "SHA1.h"
#include "SHA256.h"
#include "../Tools/Exception.h"

using namespace std;
namespace BSL
{
    namespace Crypto
    {
        //Functions titles
        constexpr byte symcount = 2;
        static char *symtitles[]
        {
            "RC4",
            "AES"
        };

        constexpr byte asymcount = 1;
        static char *asymtitles[]
        {
            "RSA",
        };

        constexpr byte hashcount = 3;
        static char *hashtitles[]
        {
            "MD5",
            "SHA1",
            "SHA256"
        };

        static ALG_ID hashid[]
        {
            CALG_MD5,
            CALG_SHA1,
            CALG_SHA_256
        };

        static size_t defaultasymkeysizes[] =
        {
            2048
        };

        static size_t defaultsymkeysizes[] =
        {
            256,
            256
        };


        //Construction functions
        unique_ptr<AsymetricEncryptor> CryptoProvider::Construct(AsymetricEncryption id)
        {
            switch (id)
            {
                case AsymetricEncryption::RSA:
                {
                    return make_unique<RSAEncryptor>();
                }
                default:
                {
                    throw Exception("Invalid AsymetricEncryption id in CryptoProvider::Construct");
                }
            }
        }

        unique_ptr<SymetricEncryptor> CryptoProvider::Construct(SymetricEncryption id)
        {
            switch (id)
            {
                case SymetricEncryption::AES:
                {
                    return make_unique<AESEncryptor>();
                }
                case SymetricEncryption::RC4:
                {
                    return make_unique<AESEncryptor>();
                }
                default:
                {
                    throw Exception("Invalid AsymetricEncryption id in CryptoProvider::Construct");
                }
            }
        }

        unique_ptr<HashBase> CryptoProvider::Construct(HashFunction id)
        {
            switch (id)
            {
                case HashFunction::MD5:
                {
                    return make_unique<MD5>();
                }
                case HashFunction::SHA1:
                {
                    return make_unique<SHA1>();
                }
                case HashFunction::SHA256:
                {
                    return make_unique<SHA256>();
                }
                default:
                {
                    throw Exception("Invalid AsymetricEncryption id in CryptoProvider::Construct");
                }
            }
        }


        //Title to id
        string CryptoProvider::GetEncryptionTitle(AsymetricEncryption id)
        {
            if (static_cast<byte>(id) >= asymcount)
            {
                return "";
            }
            return asymtitles[static_cast<BYTE>(id)];
        }

        string CryptoProvider::GetEncryptionTitle(SymetricEncryption id)
        {
            if (static_cast<byte>(id) >= symcount)
            {
                return "";
            }
            return symtitles[static_cast<BYTE>(id)];
        }

        string CryptoProvider::GetHashFunctionTitle(HashFunction id)
        {
            if (static_cast<byte>(id) >= hashcount)
            {
                return "";
            }
            return hashtitles[static_cast<BYTE>(id)];
        }


        //String to id functions
        SymetricEncryption CryptoProvider::GetSymetricEncryptionId(const string &name)
        {
            for (byte i = 0; i < symcount; i++)
            {
                if (name == symtitles[i])
                {
                    return static_cast<SymetricEncryption>(i);
                }
            }
            return SymetricEncryption::Unknown;
        }

        AsymetricEncryption CryptoProvider::GetAsymetricEncryptionId(const string &name)
        {
            for (byte i = 0; i < asymcount; i++)
            {
                if (name == asymtitles[i])
                {
                    return static_cast<AsymetricEncryption>(i);
                }
            }
            return AsymetricEncryption::Unknown;
        }

        HashFunction CryptoProvider::GetHashFunctionId(const string &name)
        {
            for (byte i = 0; i < hashcount; i++)
            {
                if (name == hashtitles[i])
                {
                    return static_cast<HashFunction>(i);
                }
            }
            return HashFunction::Unknown;
        }

        ALG_ID CryptoProvider::GetWin32HashID(HashFunction id)
        {
            if (static_cast<byte>(id) >= hashcount)
            {
                throw Exception("Invalid HashFunction in GetWin32HashID");
            }
            return hashid[static_cast<byte>(id)];
        }

        bool CryptoProvider::IsValidFunction(AsymetricEncryption function)
        {
            return static_cast<byte>(function) < asymcount;
        }

        bool CryptoProvider::IsValidFunction(SymetricEncryption function)
        {
            return static_cast<byte>(function) < symcount;
        }

        bool CryptoProvider::IsValidFunction(HashFunction function)
        {
            return static_cast<byte>(function) < hashcount;
        }

        size_t CryptoProvider::GetDefaultKeySize(AsymetricEncryption id)
        {
            if (!IsValidFunction(id))
            {
                return 0;
            }

            return defaultasymkeysizes[static_cast<uint>(id)];
        }

        size_t CryptoProvider::GetDefaultKeySize(SymetricEncryption id)
        {
            if (!IsValidFunction(id))
            {
                return 0;
            }

            return defaultsymkeysizes[static_cast<uint>(id)];
        }

        std::vector<AsymetricEncryption> CryptoProvider::GetSupportedAsymetricEncryptions()
        {
            std::vector<AsymetricEncryption> ret(asymcount);
            for (size_t i = 0; i < asymcount; i++)
            {
                ret[i] = static_cast<AsymetricEncryption>(i);
            }
            return ret;
        }

        std::vector<SymetricEncryption> CryptoProvider::GetSupportedSymetricEncryptions()
        {
            std::vector<SymetricEncryption> ret(symcount);
            for (size_t i = 0; i < symcount; i++)
            {
                ret[i] = static_cast<SymetricEncryption>(i);
            }
            return ret;
        }

        std::vector<HashFunction> CryptoProvider::GetSupportedHashFunctions()
        {
            std::vector<HashFunction> ret(hashcount);
            for (size_t i = 0; i < hashcount; i++)
            {
                ret[i] = static_cast<HashFunction>(i);
            }
            return ret;
        }
    }
}
