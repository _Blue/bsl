/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: HashBase.h
//Purpose: Represent base class for data hashing
#pragma once

#include <Windows.h>
#include "../DataStructures/SecuredVector.h"
#include "../BSLDefs.h"
namespace BSL
{
    namespace Crypto
    {
        class HashBase
        {
        public:
            HashBase();
            HashBase(HashBase &&other);
            HashBase(const HashBase& other) = delete;//Because we can't duplicate HCRYPTPROV objects
            virtual ~HashBase();

            bool Hash(const std::vector<byte> &data, std::vector<byte> &value);
            bool Hash(const SecuredVector<byte> &data, std::vector<byte> &value);
            bool Hash(const byte *data, size_t size, std::vector<byte> &value);
            std::string HashToHex(const std::vector<byte> &data);//returns the hexadecimal representation of the hash of data, return empty string on error
            virtual size_t GetHashLen() = 0;//The size in bytes of the hashed data (in raw format, not in hex format)

        protected:
            virtual const char *GetProvider();
            virtual ulong GetProviderType();
            virtual ALG_ID GetHashType() = 0;

        private:
            bool Init();

            bool _isinit;
            HCRYPTPROV _hprov;
            size_t _hashlen;
        };
    }
}