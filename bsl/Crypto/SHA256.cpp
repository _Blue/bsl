/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "SHA256.h"
namespace BSL
{
    namespace Crypto
    {
        size_t SHA256::GetHashLen()
        {
            return 32;
        }

        ALG_ID SHA256::GetHashType()
        {
            return CALG_SHA_256;
        }

        const char* SHA256::GetProvider()
        {
            return MS_ENH_RSA_AES_PROV;
        }

        DWORD SHA256::GetProviderType()
        {
            return PROV_RSA_AES;
        }
    }
}