/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "CryptoBase.h"
#include "Encoding.h"
using namespace std;
namespace BSL
{
    namespace Crypto
    {
        CryptoBase::CryptoBase()
        {
            _isinit = false;
            _key = 0;
            _hprovider = 0;
        }

        CryptoBase::CryptoBase(CryptoBase &&other)
        {
            _key = other._key;
            _hprovider = other._hprovider;
            _clearblocklen = other._clearblocklen;
            _encryptedblocklen = other._clearblocklen;
            _isinit = other._isinit;

            //Make sure that the other's object destructor won't release the provider handle or destory the key
            other._key = 0;
            other._hprovider = 0;
            other._isinit = false;
        }

        CryptoBase::~CryptoBase()
        {
            if (_key != 0)
            {
                CryptDestroyKey(_key);
            }

            if (_hprovider != 0)
            {
                CryptReleaseContext(_hprovider, 0);
            }
        }

        void CryptoBase::PrepareForNewKey()
        {
            if (_key != 0)
            {
                CryptDestroyKey(_key);
                _key = 0;
            }
        }

        bool CryptoBase::Init(DWORD provider)
        {
            _isinit = CryptAcquireContext(&_hprovider, NULL, NULL, provider, 0);
            return _isinit;
        }

        bool CryptoBase::GenerateKey(DWORD keysize)
        {
            if (!_isinit&&!Init(GetProvider()))
            {
                return false;
            }

            PrepareForNewKey();
            if (!CryptGenKey(_hprovider, GetKeyType(), MAKELONG(CRYPT_EXPORTABLE, keysize), &_key))
            {
                return false;
            }
            UpdateKeyParameters(keysize);
            return true;
        }

        bool CryptoBase::GetKey(DWORD keytype, SecuredVector<BYTE> &key)
        {
            DWORD len = 0;
            if (!CryptExportKey(_key, NULL, keytype, NULL, NULL, &len))//First call with no buffer to determine the buffer size
            {
                return false;
            }

            key.resize(len);

            return CryptExportKey(_key, NULL, keytype, NULL, key.data(), &len);
        }

        bool CryptoBase::Encrypt(SecuredVector<BYTE> &data)
        {
            auto blocks = SplitData(data, _encryptedblocklen);
            for (auto &i : blocks)//Encrypt each block
            {
                if (!EncryptBlock(i))
                {
                    return false;
                }
            }

            MergeData(blocks, data);
            return true;
        }

        bool CryptoBase::EncryptBlock(SecuredVector<BYTE> &data)
        {
            //First call the function with no buffer to get the buffer size
            DWORD originaldatasize = data.size();
            DWORD requiredsize = originaldatasize;
            if (!CryptEncrypt(_key, NULL, TRUE, NULL, NULL, &requiredsize, originaldatasize))
            {
                return false;
            }

            data.resize(requiredsize);
            requiredsize = originaldatasize;
            originaldatasize = data.size();
            if (!CryptEncrypt(_key, NULL, TRUE, NULL, data.data(), &requiredsize, originaldatasize))
            {
                return false;
            }

            return true;
        }

        bool CryptoBase::Decrypt(SecuredVector<BYTE> &data)
        {
            auto blocks = SplitData(data, _clearblocklen);
            for (auto &i : blocks)//Encrypt each block
            {
                if (!DecryptBlock(i))
                {
                    return false;
                }
            }
            MergeData(blocks, data);
            return true;
        }

        bool CryptoBase::DecryptBlock(SecuredVector<BYTE> &data)
        {
            DWORD datasize = data.size();
            if (!CryptDecrypt(_key, NULL, TRUE, NULL, data.data(), &datasize))
            {
                return false;
            }
            data.resize(datasize);
            return true;
        }


        vector<SecuredVector<BYTE>> CryptoBase::SplitData(const SecuredVector<BYTE> &data, DWORD blocsize)//Splits data in small vector that match the size of a block
        {
            vector<SecuredVector<BYTE>> blocks;
            for (size_t i = 0; i < data.size(); i += blocsize)//Split the data in blocks of size 'blocksize'
            {
                blocks.push_back(SecuredVector<BYTE>(data.begin() + i, data.begin() + i + min(blocsize, data.size() - i)));
            }
            return blocks;
        }

        void CryptoBase::MergeData(const std::vector<SecuredVector<BYTE>> &src, SecuredVector<BYTE> &target)
        {
            size_t totalsize = 0;
            for (auto &i : src)//Compute the whole data size
            {
                totalsize += i.size();
            }

            target.resize(totalsize);//Resize to destination 
            size_t pos = 0;
            for (auto &i : src)//Merge in the result buffer
            {
                copy(i.begin(), i.end(), target.begin() + pos);
                pos += i.size();
            }
        }

        void CryptoBase::UpdateKeyParameters(DWORD keysize)//Default function, called when blocked encryption is not needed
        {
            _encryptedblocklen = 0;
            _clearblocklen = 0;
        }

        bool CryptoBase::GetRandom(SecuredVector<BYTE> &data)
        {
            if (data.size() == 0 || (!_isinit && !Init(GetProvider())))
            {
                return false;
            }

            return CryptGenRandom(_hprovider, data.size(), data.data());
        }
    }
}