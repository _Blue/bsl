/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "HashBase.h"
#include "Encoding.h"

using namespace std;

namespace BSL
{
    namespace Crypto
    {
        HashBase::HashBase()
        {
            _isinit = false;
        }

        HashBase::HashBase(HashBase &&other)
        {
            _isinit = other._isinit;
            if (_isinit)
            {
                _hprov = other._hprov;
                _hashlen = other._hashlen;
                other._isinit = false;
            }
        }

        HashBase::~HashBase()
        {
            if (_isinit)
            {
                CryptReleaseContext(_hprov, 0);
            }
        }

        bool HashBase::Hash(const vector<byte> &data, vector<byte> &value)
        {
            return Hash(data.data(), data.size(), value);
        }

        bool HashBase::Hash(const SecuredVector<byte> &data, vector<byte> &value)
        {
            return Hash(data.data(), data.size(), value);
        }

        bool HashBase::Hash(const byte *data, size_t size, vector<byte> &value)
        {
            if (!_isinit&&!Init())//Initialize if it has not been before
            {
                return false;
            }

            HCRYPTHASH hash;
            if (!CryptCreateHash(_hprov, GetHashType(), 0, 0, &hash))//Create the hash object
            {
                return false;
            }

            if (!CryptHashData(hash, data, size, 0))//Do the actual hash
            {
                return false;
            }

            DWORD datalen = GetHashLen();
            value.resize(datalen);//resize the vector to the hash data size

            if (!CryptGetHashParam(hash, HP_HASHVAL, value.data(), &datalen, 0))//Get the hash value
            {
                return false;
            }
            
            CryptDestroyHash(hash);//Release the hash
            return true;
        }

        string HashBase::HashToHex(const vector<byte> &data)
        {
            vector<byte> hashvalue;
            if (!Hash(data, hashvalue))
            {
                return "";
            }
            return Encoding::EncodeToHex(hashvalue);
        }

        const char *HashBase::GetProvider()
        {
            return nullptr;//default provider
        }

        bool HashBase::Init()
        {
            if (!CryptAcquireContext(&_hprov, nullptr, GetProvider(), GetProviderType(), CRYPT_VERIFYCONTEXT))
            {
                return false;
            }

            _isinit = true;
            _hashlen = GetHashLen();
            return true;
        }

        DWORD HashBase::GetProviderType()
        {
            return PROV_RSA_FULL;
        }
    }
}