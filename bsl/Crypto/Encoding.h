/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: Encoding.h
//Purpose: Provide data representation conversions

#pragma once

#include <string>
#include <vector>
#include "../BSLDefs.h"
#pragma comment(lib, "Crypt32.lib")
namespace BSL
{
    namespace Crypto
    {
        class Encoding
        {
        public:
            //Base 64 
            static std::string EncodeToBase64(const std::vector<byte> &data);
            static std::string EncodeToBase64(const byte *data, size_t size);
            static std::vector<byte> DecodeFromBase64(const std::string &data);

            //Hex
            static std::string EncodeToHex(const std::vector<byte> &data, bool caps = false);
            static std::string EncodeToHex(const byte *data, size_t size, bool caps = false);
            static std::vector<byte> DecodeFromHex(const std::string &data);
            
        private:
            static std::string Encode(const byte *data, size_t size, ulong format);
            static std::vector<byte> Decode(const std::string &data, ulong format);
            static ushort HexCharValue(char c);//Returns the value of c in a hex representation, returns -1 on error

        };
    }
}
