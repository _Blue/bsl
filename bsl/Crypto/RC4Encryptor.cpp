/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "RC4Encryptor.h"

using namespace std;
namespace BSL
{
    namespace Crypto
    {
        DWORD RC4Encryptor::GetProvider()
        {
            return PROV_RSA_FULL;
        }

        DWORD RC4Encryptor::GetKeyType()
        {
            return CALG_RC4;
        }

        bool RC4Encryptor::Encrypt(SecuredVector<BYTE> &data)
        {
            return EncryptBlock(data);
        }

        bool RC4Encryptor::Decrypt(SecuredVector<BYTE> &data)
        {
            return DecryptBlock(data);
        }

        void RC4Encryptor::UpdateKeyParameters(DWORD)
        {
            _encryptedblocklen = 40960;//A RC4 block size must be divisible by 8
            _clearblocklen = 40960;
        }

        size_t RC4Encryptor::GetDefaultKeySize() const
        {
            return 2048;
        }
    }
}