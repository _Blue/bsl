/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: CryptoBase.h
//Purpose: Provide base class for cryptographic implentations

#pragma once
#include <Windows.h>
#include <wincrypt.h>
#include <vector>
#include <iostream>
#include "../DataStructures/SecuredVector.h"

#pragma comment(lib, "Crypt32.lib")


namespace BSL
{
    namespace Crypto
    {
        class CryptoBase
        {
        public:
            CryptoBase();
            CryptoBase(const CryptoBase&) = delete;
            CryptoBase(CryptoBase &&other);
            virtual ~CryptoBase();

            virtual bool Encrypt(SecuredVector<BYTE> &data);
            virtual bool Decrypt(SecuredVector<BYTE> &data);
            virtual bool GenerateKey(DWORD keysize);

            virtual size_t GetDefaultKeySize() const = 0;
            
            bool GetRandom(SecuredVector<BYTE> &data);

        protected:
            bool Init(DWORD provider);
            bool GetKey(DWORD keytype, SecuredVector<BYTE> &key);//Keytype can be a public or private key blob in case of asymetric encryption, or a simple key blob
            bool EncryptBlock(SecuredVector<BYTE> &data);
            bool DecryptBlock(SecuredVector<BYTE> &data);
            
            void PrepareForNewKey();//Deletes an eventual old key
            virtual void UpdateKeyParameters(DWORD keysize = 0);//Updates _encryptedblocklen and _clearblocklen
            virtual DWORD GetProvider() = 0;
            virtual DWORD GetKeyType() = 0;

            HCRYPTPROV _hprovider;
            HCRYPTKEY _key;
            bool _isinit;
            DWORD _encryptedblocklen;//The size of an encrypted block
            DWORD _clearblocklen;

        private:
            std::vector<SecuredVector<BYTE>> SplitData(const SecuredVector<BYTE> &data, DWORD blocksize);//Splits data in small vector that match the size of a block
            void MergeData(const std::vector<SecuredVector<BYTE>> &src, SecuredVector<BYTE> &target);

        };
    }
}