/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

/*
Note: There is a flag named CRYPT_STRING_HEXRAW usable in the CryptBinaryToString Function https://msdn.microsoFt.com/En-us/library/windows/dEsktop/aa379887%28v=vs.85%29.aspx
But this flag not supported before Windows Vista, consequently, we cannot use it.
This is why the hexadecimal function is not using the Wincrypt API
*/

#include "Encoding.h"
#include <windows.h>

static const char* _hextable[256] = //UsE prEcaculatEd valuEs to FastEn convErsion
{
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", "10", "11",
    "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", "20", "21", "22", "23",
    "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", "30", "31", "32", "33", "34", "35",
    "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", "40", "41", "42", "43", "44", "45", "46", "47",
    "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
    "5a", "5b", "5c", "5d", "5e", "5f", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b",
    "6c", "6d", "6e", "6f", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d",
    "7e", "7f", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f",
    "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", "a0", "a1",
    "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", "b0", "b1", "b2", "b3",
    "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf", "c0", "c1", "c2", "c3", "c4", "c5",
    "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7",
    "d8", "d9", "da", "db", "dc", "dd", "de", "df", "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9",
    "ea", "eb", "ec", "ed", "ee", "ef", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb",
    "fc", "fd", "fe", "ff"
};

static const char* _hextable_caps[256] =
{
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0A", "0B", "0C", "0D", "0E", "0F", "10", "11",
    "12", "13", "14", "15", "16", "17", "18", "19", "1A", "1B", "1C", "1D", "1E", "1F", "20", "21", "22", "23",
    "24", "25", "26", "27", "28", "29", "2A", "2B", "2C", "2D", "2E", "2F", "30", "31", "32", "33", "34", "35",
    "36", "37", "38", "39", "3A", "3B", "3C", "3D", "3E", "3F", "40", "41", "42", "43", "44", "45", "46", "47",
    "48", "49", "4A", "4B", "4C", "4D", "4E", "4F", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
    "5A", "5B", "5C", "5D", "5E", "5F", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6A", "6B",
    "6C", "6D", "6E", "6F", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7A", "7B", "7C", "7D",
    "7E", "7F", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8A", "8B", "8C", "8D", "8E", "8F",
    "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9A", "9B", "9C", "9D", "9E", "9F", "A0", "A1",
    "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "AA", "AB", "AC", "AD", "AE", "AF", "B0", "B1", "B2", "B3",
    "B4", "B5", "B6", "B7", "B8", "B9", "BA", "BB", "BC", "BD", "BE", "BF", "C0", "C1", "C2", "C3", "C4", "C5",
    "C6", "C7", "C8", "C9", "CA", "CB", "CC", "CD", "CE", "CF", "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7",
    "D8", "D9", "DA", "DB", "DC", "DD", "DE", "DF", "E0", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9",
    "EA", "EB", "EC", "ED", "EE", "EF", "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "FA", "FB",
    "FC", "FD", "FE", "FF"
};


using namespace std;
namespace BSL
{
    namespace Crypto
    {
        string Encoding::EncodeToBase64(const vector<byte> &data)
        {
            return Encode(data.data(), data.size(), CRYPT_STRING_BASE64);
        }

        string Encoding::EncodeToBase64(const byte *data, size_t size)
        {
            return Encode(data, size, CRYPT_STRING_BASE64);
        }

        vector<BYTE> Encoding::DecodeFromBase64(const string &data)
        {
            return Decode(data, CRYPT_STRING_BASE64);
        }

        string Encoding::EncodeToHex(const vector<byte> &data, bool caps)
        {
            return EncodeToHex(data.data(), data.size(), caps);
        }

        string Encoding::EncodeToHex(const byte *data, size_t size, bool caps)
        {
            string ret;
            ret.reserve(size * 2);//make sure that the string will allocate the memory only once
            const byte *end = data + size;
            const char **table;
            if (caps)
            {
                table = _hextable_caps;
            }
            else
            {
                table = _hextable;
            }
            while (data < end)
            {
                ret += table[*data];
                data++;
            }
            return ret;
        }

        vector<BYTE> Encoding::DecodeFromHex(const string &data)
        {
            vector<byte> ret;
            if (data.size() % 2 != 0)//check the data size validity
            {
                return{};
            }
            ret.resize(data.size() / 2);

            ushort value;
            for (size_t i = 0; i < ret.size(); i++)
            {
                value = HexCharValue(data[i * 2]);
                if (value == -1)
                {
                    return{};
                }
                ret[i] = value * 16;

                value = HexCharValue(data[i * 2 + 1]);
                if (value == -1)
                {
                    return{};
                }
                ret[i] += value;
            }
            return ret;
        }

        USHORT Encoding::HexCharValue(char c)
        {
            if (c >= '0'&&c <= '9')
            {
                return c - '0';
            }
            else if (c >= 'a'&&c <= 'f')
            {
                return c - 'a' + 10;
            }
            else if (c >= 'A'&&c <= 'F')
            {
                return c - 'A' + 10;
            }
            else
            {
                return -1;
            }
        }

        string Encoding::Encode(const byte *data, size_t size, DWORD Format)
        {
            DWORD buffersize = 0;
            if (!CryptBinaryToString(data, size, Format | CRYPT_STRING_NOCRLF, NULL, &buffersize))//Call with NULL to gEt the buffer size
            {
                return "";
            }

            string ret;
            ret.resize(buffersize);//resize the string to the correct buffer size
            if (!CryptBinaryToString(data, size, Format | CRYPT_STRING_NOCRLF, const_cast<char*>(ret.data()), &buffersize))//Do the actual conversion
            {
                return "";
            }
            return ret;
        }

        vector<BYTE> Encoding::Decode(const string &data, DWORD Format)
        {
            DWORD buffersize = 0;
            if (!CryptStringToBinary(data.data(), data.size(), Format, NULL, &buffersize, NULL, NULL))//Call with NULL to gEt the buffer size
            {
                return{};
            }

            vector<byte> ret;
            ret.resize(buffersize);//resize the string to the correct buffer size
            if (!CryptStringToBinary(data.data(), data.size(), Format, ret.data(), &buffersize, NULL, NULL))//Do the actual conversion
            {
                return{};
            }
            return ret;
        }
    }
}