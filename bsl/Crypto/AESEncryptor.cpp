/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "AESEncryptor.h"

using namespace std;
namespace BSL
{
    namespace Crypto
    {
        DWORD AESEncryptor::GetProvider()
        {
            return PROV_RSA_AES;
        }

        DWORD AESEncryptor::GetKeyType()
        {
            return CALG_AES_256;
        }

        bool AESEncryptor::Encrypt(SecuredVector<BYTE> &data)
        {
            return EncryptBlock(data);//In AES, we can consider the whole data as one big block
        }

        bool AESEncryptor::Decrypt(SecuredVector<BYTE> &data)
        {
            return DecryptBlock(data);
        }

        size_t AESEncryptor::GetDefaultKeySize() const
        {
            return 256;
        }
    }
}