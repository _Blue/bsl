/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "SymetricEncryptor.h"
#include "CryptoProvider.h"

using namespace std;
namespace BSL
{
    namespace Crypto
    {
        bool SymetricEncryptor::SetKey(const SecuredVector<BYTE>& key)
        {
            if (!Init(GetProvider())||!CryptImportKey(_hprovider, key.data(), key.size(), NULL, NULL, &_key))
            {
                return false;
            }
            UpdateKeyParameters();
            return true;
        }

        bool SymetricEncryptor::GetKey(SecuredVector<BYTE> &key)
        {
            return CryptoBase::GetKey(PLAINTEXTKEYBLOB, key);
        }

        bool SymetricEncryptor::DeriveKey(const SecuredVector<BYTE> &data, DWORD keysize, HashFunction hashfunction)
        {
            if (!_isinit&&!Init(GetProvider()))
            {
                return false;
            }

            PrepareForNewKey();

            HCRYPTHASH hash;
            if (!CryptCreateHash(_hprovider, CryptoProvider::GetWin32HashID(hashfunction), 0, 0, &hash))
            {
                return false;
            }

            if (!CryptHashData(hash, data.data(), data.size(), 0))
            {
                CryptDestroyHash(hash);
                return false;
            }

            if (!CryptDeriveKey(_hprovider, GetKeyType(), hash, MAKELONG(CRYPT_EXPORTABLE, keysize), &_key))
            {
                CryptDestroyHash(hash);
                return false;
            }
            CryptDestroyHash(hash);
            UpdateKeyParameters(keysize);
            return true;
        }
    }
}