/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#pragma once

#include "CryptoBase.h"
namespace BSL
{
    namespace Crypto
    {
        enum class HashFunction : BYTE;
        class SymetricEncryptor :public CryptoBase
        {
        public:
            SymetricEncryptor() = default;
            bool SetKey(const SecuredVector<BYTE> &key);
            bool GetKey(SecuredVector<BYTE> &key);

            //Note, we use a static cast to set the default value because we can't include CryptoProvider here, 
            //It would cause a loop in inclusion
            //It is guaranteed that same data will generate the same keys
            virtual bool DeriveKey(const SecuredVector<BYTE> &data, DWORD keysize, HashFunction hashfunction = static_cast<HashFunction>(0));//0 means default

        };
    }
}
