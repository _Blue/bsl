/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: CryptoProvider.h
//Purpose: Enumerates encryption and signing methods that are available 
#pragma once
#include <memory>
#include "SymetricEncryptor.h"
#include "AsymetricEncryptor.h"
#include "HashBase.h"
#include "../BSLDefs.h"

namespace BSL
{
    namespace Crypto
    {
        enum class SymetricEncryption : byte
        {
            Default = 0,
            AES = 0,
            RC4 = 1,
            None = 254,
            Unknown = 255
        };

        enum class AsymetricEncryption : byte
        {
            Default = 0,
            RSA = 0,
            None = 254,
            Unknown = 255
        };

        enum class HashFunction : byte
        {
            Default = 0,
            SHA256 = 0,
            SHA1 = 1,
            MD5 = 2,
            None = 254,
            Unknown = 255
        };

        class CryptoProvider
        {
        public:
            static std::unique_ptr<SymetricEncryptor> Construct(SymetricEncryption id);
            static std::unique_ptr<AsymetricEncryptor> Construct(AsymetricEncryption id);
            static std::unique_ptr<HashBase> Construct(HashFunction id);

            static std::string GetEncryptionTitle(AsymetricEncryption id);
            static std::string GetEncryptionTitle(SymetricEncryption id);
            static std::string GetHashFunctionTitle(HashFunction id);

            static SymetricEncryption GetSymetricEncryptionId(const std::string &name);
            static AsymetricEncryption GetAsymetricEncryptionId(const std::string &name);
            static HashFunction GetHashFunctionId(const std::string &name);

            static bool IsValidFunction(AsymetricEncryption function);
            static bool IsValidFunction(SymetricEncryption function);
            static bool IsValidFunction(HashFunction function);

            static size_t GetDefaultKeySize(AsymetricEncryption id);
            static size_t GetDefaultKeySize(SymetricEncryption id);

            static ALG_ID GetWin32HashID(HashFunction id);

            static std::vector<AsymetricEncryption> GetSupportedAsymetricEncryptions();
            static std::vector<SymetricEncryption> GetSupportedSymetricEncryptions();
            static std::vector<HashFunction> GetSupportedHashFunctions();
        };
    }
}
