/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "RSAEncryptor.h"

using namespace std;
namespace BSL
{
    namespace Crypto
    {
        RSAEncryptor::RSAEncryptor() : AsymetricEncryptor()
        {

        }

        void RSAEncryptor::UpdateKeyParameters(DWORD keysize)
        {
            if (keysize == 0)
            {
                DWORD len = sizeof(DWORD);
                CryptGetKeyParam(_key, KP_KEYLEN, reinterpret_cast<BYTE*>(&keysize), &len, 0);
            }
            //The RSA block len is the size of the key modulus - 11
            //Keysize is the size of the key modulus in bits, to we divide it by 8
            _encryptedblocklen = (keysize / 8) - 11;
            _clearblocklen = keysize / 8;
        }

        DWORD RSAEncryptor::GetProvider()
        {
            return PROV_RSA_FULL;
        }

        DWORD RSAEncryptor::GetKeyType()
        {
            return AT_KEYEXCHANGE;
        }

        size_t RSAEncryptor::GetDefaultKeySize() const
        {
            return 2048;
        }
    }
}