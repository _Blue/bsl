/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "AsymetricEncryptor.h"

using namespace std;
namespace BSL
{
    namespace Crypto
    {
        bool AsymetricEncryptor::GetPrivateKey(SecuredVector<BYTE> &key)
        {
            return GetKey(PRIVATEKEYBLOB, key);
        }

        bool AsymetricEncryptor::GetPublicKey(SecuredVector<BYTE> &key)
        {
            return GetKey(PUBLICKEYBLOB, key);
        }

        bool AsymetricEncryptor::SetPublicKey(const SecuredVector<BYTE> &key)
        {
            if ((!_isinit &&!Init(PROV_RSA_FULL)) || !CryptImportKey(_hprovider, key.data(), key.size(), NULL, NULL, &_key))
            {
                return false;
            }
            UpdateKeyParameters();
            return true;
        }

        bool AsymetricEncryptor::SetPrivateKey(const SecuredVector<BYTE> &key)
        {
            if ((!_isinit &&!Init(PROV_RSA_FULL)) || !CryptImportKey(_hprovider, key.data(), key.size(), NULL, NULL, &_key))
            {
                return false;
            }
            UpdateKeyParameters();
            return true;
        }
    }
}