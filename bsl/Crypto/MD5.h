/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: MD5.h
//Purpose: Subclass of HashBase, provide MD5 hash calculation

#include "HashBase.h"
namespace BSL
{
    namespace Crypto
    {
        class MD5 : public HashBase
        {
        public:
            MD5() = default;
            size_t GetHashLen();
        
        protected:
            ALG_ID GetHashType();
        };
    }
}