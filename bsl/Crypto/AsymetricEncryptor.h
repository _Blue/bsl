/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#pragma once

#include "CryptoBase.h"
namespace BSL
{
    namespace Crypto
    {
        class AsymetricEncryptor :public CryptoBase
        {
        public:
            AsymetricEncryptor() = default;
            bool SetPrivateKey(const SecuredVector<BYTE> &key);
            bool SetPublicKey(const SecuredVector<BYTE> &key);
            bool GetPublicKey(SecuredVector<BYTE> &key);
            bool GetPrivateKey(SecuredVector<BYTE> &key);
        };
    }
}
