/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: Exception.h
//Purpose: Represent the BSL exception class
#pragma once

#include <exception>
#include <string>
#include <memory>

namespace BSL
{
  class Exception : public std::exception
  {
  public:
    explicit Exception(const char *what);
    virtual ~Exception() = default;

  };
}