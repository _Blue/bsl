/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "TCPClient.h"

using namespace std;
using namespace BSL::Network;
namespace BSL
{
    namespace Network
    {
        TCPClient::TCPClient(SOCKET socket) : TCPBase(socket)
        {

        }

        TCPError TCPClient::Connect(const string &host, ushort port)
        {
            TCPError ret = InitWinsock(true);
            if (ret != TCPError::Sucess)
            {
                return ret;
            }

            //Setup client informations
            sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_addr.s_addr = inet_addr(host.c_str());
            addr.sin_port = htons(port);

            //Call connect
            if (connect(_socket, reinterpret_cast<const sockaddr*>(&addr), sizeof(sockaddr_in)) == SOCKET_ERROR)
            {
                switch (WSAGetLastError())
                {
                    case 0:
                    {
                        return TCPError::Sucess;
                    }
                    case WSAECONNREFUSED://Host found, but port closed
                    {
                        return TCPError::EConnectionRefused;
                    }
                    case WSAETIMEDOUT://Host did not answer
                    {
                        return TCPError::EUnknownHost;
                    }
                    default://Other error, considering it should not happen
                    {
                        return TCPError::EWSAError;
                    }
                }
            }
            return TCPError::Sucess;
        }
    }
}