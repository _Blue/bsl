/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include <limits.h>
#include "TCPServer.h"

#define MAX_TCP_QUEUE 5

namespace BSL
{
    namespace Network
    {
        TCPServer::TCPServer()
        {

        }

        TCPError TCPServer::Start(ushort port, ClientHandler &handler, const std::string &listenaddress)
        {
            //Get local host
            char hostname[NI_MAXHOST];
            if (gethostname(hostname, NI_MAXHOST) == SOCKET_ERROR)
            {
                return TCPError::EWSAError;
            }

            //initialize structs
            sockaddr_in address;

            //If no listen address specified, use the first (default) address
            if (listenaddress.empty())
            {
                hostent *host = gethostbyname(hostname);
                if (host == NULL)
                {
                    return TCPError::EWSAError;
                }

                memcpy(&address.sin_addr.s_addr, host->h_addr_list[0], sizeof(long));
            }
            else
            {
                long addr = inet_addr(listenaddress.c_str());
                memcpy(&address.sin_addr.s_addr, &addr, sizeof(long));

            }
            address.sin_family = AF_INET;
            address.sin_port = htons(port);

            SOCKET listensocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            if (listensocket == INVALID_SOCKET)
            {
                return TCPError::EWSAError;
            }

            //Bind the socket
            if (bind(listensocket, reinterpret_cast<sockaddr*>(&address), sizeof(sockaddr)) == SOCKET_ERROR)
            {
                closesocket(listensocket);
                return TCPError::EListenFailure;
            }

            //Listen on the port
            if (listen(listensocket, MAX_TCP_QUEUE) == SOCKET_ERROR)
            {
                closesocket(listensocket);
                return TCPError::EListenFailure;
            }

            //Wait for the clients
            sockaddr_in clientaddr;
            int clientlen = sizeof(sockaddr_in);
            bool wantmore;
            do
            {
                SOCKET clientsocket = accept(listensocket, reinterpret_cast<sockaddr*>(&clientaddr), &clientlen);

                closesocket(listensocket);
                if (clientsocket == INVALID_SOCKET)
                {
                    return TCPError::EAcceptFailure;
                }

                //Save the client informations (address and high level port)
                ClientInfos client;
                client.hostname = inet_ntoa(clientaddr.sin_addr);
                client.port = clientaddr.sin_port;
                client.socket = clientsocket;
                wantmore = handler.ClientConnected(client);
            } while (wantmore);

            return TCPError::Sucess;
        }
    }
}