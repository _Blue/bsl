/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: ClientHandler.h
//Purpose: Abstract class used to accept client from a TCPServer

#pragma once
#include "ClientInfos.h"
namespace BSL
{
    namespace Network
    {
        class ClientHandler
        {
        public:
            //This function is called each time a client is connected
            //Note that this call must not be blocking, if it is, the associated TCPServer will be stuck
            //client will be a valid pointer until the return of this function, after it will be deleted
            virtual bool ClientConnected(const ClientInfos &client) = 0;//Return true if you want the TCPServer to continue listenning, false otherwise.
        };
    }
}
