/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: TCPError.h
//Purpose: Contains an enum describing errors that may occur within TCP connexions

#pragma once
namespace BSL
{
    namespace Network
    {
        class TCPError
        {
        public:
            enum
            {
                Sucess,
                EConnectionTimeout,
                EConnectionRefused,//The host was found, but the port was closed
                EConnectionClosed,//Connection was closed
                EProtocolMismatch,
                EUnknownHost,//Failed to resolve hostname
                EListenFailure,
                EAcceptFailure,
                EPacketTooBig,
                EEncryptionError,//Error while encryption or decryption
                EInvalidSignature,//The message has an invalid signature
                ENegociationFailure,//When the SecuredTCP negociation failed  (the remote host does not support good functions)
                EMessageTransportFailure,//When a message could not be sent or received while establishing a secured context
                EWSAError//Winsock internal error
            } error;

            explicit operator bool()//Implicit boolean conversion
            {
                return error == TCPError::Sucess;
            }

            TCPError(decltype(error) value) : error(value)
            {

            }
        };

        inline bool operator==(TCPError a, TCPError b)
        {
            return a.error == b.error;
        }

        inline bool operator!=(TCPError a, TCPError b)
        {
            return !(a == b);
        }
    }
}