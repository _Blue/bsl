/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: SecuredTCPOptionsHelper.h
//Purpose: Class that contains data representations to easily read SecuredTCP packets

#pragma 
#include "../BSLDefs.h"
namespace BSL
{
    namespace Crypto
    {
        enum class AsymetricEncryption : unsigned char;
        enum class SymetricEncryption : unsigned char;
        enum class HashFunction : unsigned char;
    }

    namespace Network
    {
        namespace Helpers//Internal code, no need to show it the Network namespace
        {
            template<typename T>
            struct EncyptionOptions
            {
                EncyptionOptions()
                {

                }

                EncyptionOptions(T alg, size_t min, size_t max)
                {
                    algorithm = alg;
                    minkeysize = min;
                    maxkeysize = max;
                }

                template <typename Tother>
                EncyptionOptions(const Tother params)
                {
                    algorithm = static_cast<T>(params.algorithm);
                    minkeysize = params.minkeysize;
                    maxkeysize = params.maxkeysize;
                }

                bool operator<(EncyptionOptions<T> another)
                {
                    return algorithm < another.algorithm;
                }

                template <typename Tother>
                bool operator==(const Tother other) const//Overloading this operator allows us to use the same function for encryption and hashing
                {
                    return other.algorithm == algorithm && ((minkeysize >= other.minkeysize && minkeysize <= other.maxkeysize) || (maxkeysize >= other.minkeysize && maxkeysize <= other.maxkeysize));
                }

                T algorithm;//The identifyer of the algorithm
                uint minkeysize;//Minimum accepted key size
                uint maxkeysize;//Maximum accepted key size
            };

            struct AlgorithmParams
            {
                template<typename T>
                AlgorithmParams(const EncyptionOptions<T> &params)
                {
                    algorithm = static_cast<byte>(params.algorithm);
                    minkeysize = params.minkeysize;
                    maxkeysize = params.maxkeysize;
                }

                byte algorithm;
                uint minkeysize;
                uint maxkeysize;
            };

            struct ParameterHeader
            {
                byte version;//the version of the procotol, set by default to BSL_SECUREDTCPVERSION
                byte asymetric;//Number of supported asymetric encryption algorithms
                byte symetric;//Number of supported symetric encryption algorithm
                byte hash;//Number of supported hash functions
                AlgorithmParams algs[1];//The list of algorithms
            };

            template<typename T>
            bool GetBestMatch(const std::vector<T> &accepted, const std::vector<T> &ours, T *elocal, T *eremote = nullptr)//returns the best algorithm that matches accepted and has been added
            {
                for (const auto & eaccepted : accepted)//Then search for a match in the two arrays
                {
                    for (const auto &eours : ours)
                    {
                        if (eours == eaccepted)//Check for a match in algorithms
                        {
                            *elocal = eours;
                            if (eremote != nullptr)
                            {
                                *eremote = eaccepted;
                            }
                            return true;
                        }
                    }
                }
                return false;//If no match were found
            }
        }

    };
}