/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

//File: TCPBase.h
//Purpose: Contains TCPBase class, an abstract class that implements basic tcp operations, such as sending or receiving data

#pragma once

#include <WinSock2.h>
#include <string>
#include "../DataStructures/SecuredVector.h"
#include "TCPError.h"
#include "../BSLDefs.h"

#define DEFAULT_RECEIVE_BUFFER 8192
#pragma comment(lib, "WS2_32.lib")//Link with Winsock library

namespace BSL
{
    namespace Network
    {
        class TCPBase
        {
        public:
            TCPBase(SOCKET socket = 0);
            TCPBase(const TCPBase &) = delete;//We can't create copy of SOCKETS
            TCPBase(TCPBase &&other);

            virtual ~TCPBase();

            TCPError Send(const std::string &data);
            TCPError Send(const SecuredVector<byte> &data);
            virtual TCPError Send(const byte *data, size_t datasize);

            virtual TCPError Receive(SecuredVector<byte> &buffer, size_t datasize);//Receives at most datasize bytes
            std::string ReceiveStr(size_t buffersize = DEFAULT_RECEIVE_BUFFER);//Receives a string, returns empty string on error

            //Returns the high level connection port, valid only if the connection is established
            ushort GetHighConnectionPort();//Returns 0 on error

            /*
            Returns the connected SOCKET of the instance
            After this call, the instance of the object does not
            have the SOCKET anymore so that the socket does not get closed
            when object is destroyed.
            Throws a BSL::Exception if the instance is not connected
            */
            SOCKET ExtractSocket();
            void Close();

        protected:
            TCPError SendImpl(const byte *data, size_t datasize);//Function that actually sends data, used to avoid stack overflow in sub-classes when send is called
            TCPError ReceiveImpl(byte *buffer, uint &size);//Receive at most "size" bytes, after the call, size hold the number of byte that have been read
            TCPError InitWinsock(bool createsocket);
            SOCKET _socket;

        private:


        };
    }
}