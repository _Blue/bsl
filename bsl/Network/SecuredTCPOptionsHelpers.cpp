/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "SecuredTCPOptions.h"
#include "../Crypto/CryptoProvider.h"
#define BSL_SECUREDTCPVERSION 1

using namespace BSL::Crypto;
using namespace std;
namespace BSL
{
    namespace Network
    {
        SecuredTCPOptions::SecuredTCPOptions(bool defaultinit)
        {
            if (defaultinit)
            {
                _asymetricalgs = { EncyptionOptions<AsymetricEncryption>(AsymetricEncryption::Default, 0 ,0) };
                _symetricalgs = { EncyptionOptions<SymetricEncryption>(SymetricEncryption::Default, 0, 0) };
            };
            _hashfunctions = { HashFunction::None };
        }

        void  SecuredTCPOptions::AddAllowedEncryption(AsymetricEncryption alg, size_t minsize, size_t maxsize)
        {
            if (!CryptoProvider::IsValidFunction(alg))
            {
                throw Exception("Invalid cryptographic function");
            }
            if (std::find_if(_asymetricalgs.begin(), _asymetricalgs.end(), [&](EncyptionOptions<AsymetricEncryption> &e)
            {
                if (e.algorithm == alg)//If the algorithm if found, update key sizes
                {
                    e.minkeysize = minsize;
                    e.maxkeysize = maxsize;
                    return true;
                }
                return false;
            }) == _asymetricalgs.end())//Add only if the value if not already in the vector
            {
                _asymetricalgs.push_back(EncyptionOptions<AsymetricEncryption>(alg, minsize, maxsize));
            }
        }

        void  SecuredTCPOptions::AddAllowedEncryption(SymetricEncryption alg, size_t minsize, size_t maxsize)
        {
            if (!CryptoProvider::IsValidFunction(alg))
            {
                throw Exception("Invalid cryptographic function");
            }
            if (std::find_if(_symetricalgs.begin(), _symetricalgs.end(), [&](EncyptionOptions<SymetricEncryption> &e)
            {
                if (e.algorithm == alg)//If the algorithm if found, update key sizes
                {
                    e.minkeysize = minsize;
                    e.maxkeysize = maxsize;
                    return true;
                }
                return false;
            }) == _symetricalgs.end())//Add only if the value if not already in the vector
            {
                _symetricalgs.push_back(EncyptionOptions<SymetricEncryption>(alg, minsize, maxsize));
            }
        }

        void  SecuredTCPOptions::AddAllowedHashFunction(HashFunction alg)
        {
            if (!CryptoProvider::IsValidFunction(alg))
            {
                throw Exception("Invalid cryptographic function");
            }
            if (std::find(_hashfunctions.begin(), _hashfunctions.end(), alg) == _hashfunctions.end())//Add only if the value if not already in the vector
            {
                _hashfunctions.push_back(alg);
            }
        }

        AsymetricEncryption SecuredTCPOptions::GetBestAsymetricEncryption(const std::vector<Crypto::AsymetricEncryption> &accepted) const
        {
            return GetBestMatch(accepted, _asymetricalgs);
        }

        SymetricEncryption SecuredTCPOptions::GetBestSymetricEncryption(const std::vector<Crypto::SymetricEncryption> &accepted) const
        {
            return GetBestMatch(accepted, _symetricalgs);
        }

        HashFunction SecuredTCPOptions::GetBestHashFunction(const std::vector<Crypto::HashFunction> &accepted) const
        {
            return GetBestMatch(accepted, _hashfunctions);
        }

        void SecuredTCPOptions::SetSecret(const SecuredVector<byte> &secret)
        {
            if (std::find_if(_hashfunctions.cbegin(), _hashfunctions.cend(), [](const HashFunction &e)//Check that at least one hash function is accepted
            {
                return e != HashFunction::None;
            }) == _hashfunctions.cend())
            {
                throw Exception("SetSecret was called without a valid hash function set");
            }
            _secret = secret;
        }

        const SecuredVector<byte> &SecuredTCPOptions::GetSecret() const
        {
            return _secret;
        }

        vector<byte> SecuredTCPOptions::GetParameterPacket() const
        {
            //Data format: 
            //[UBYTE] Protocol version number
            //[UBYTE] Number of accepted asymetric encryptions
            //[UBYTE] Number of accepted symetric encryptions
            //[UBYTE] Number of accepted hash functions
            //[UBYTE] Minimal size of asymetric key
            //
            //[UBYTE*] Supported functions,  each function is represented by a UBYTE
            //...

            std::vector<byte> ret;
            ret.reserve(sizeof(byte) * 4 + _asymetricalgs.size() + _symetricalgs.size() + _hashfunctions.size());//Avoid multiple allocations
            ret.push_back(BSL_SECUREDTCPVERSION);//Set the version the version number
            ret.push_back(static_cast<byte>(_asymetricalgs.size()));//Set the asymetric encryption functions number
            ret.push_back(static_cast<byte>(_symetricalgs.size()));//Set the symetric encryption functions number
            ret.push_back(static_cast<byte>(_hashfunctions.size()));//Set the hash functions number

            ret.insert(ret.end(), reinterpret_cast<const byte*>(_asymetricalgs.data()), reinterpret_cast<const byte*>(_asymetricalgs.data()) + _asymetricalgs.size());//Add the asymetric encryption functions
            ret.insert(ret.end(), reinterpret_cast<const byte*>(_symetricalgs.data()), reinterpret_cast<const byte*>(_symetricalgs.data()) + _symetricalgs.size());//Add the symetric encryption functions number
            ret.insert(ret.end(), reinterpret_cast<const byte*>(_hashfunctions.data()), reinterpret_cast<const byte*>(_hashfunctions.data()) + _hashfunctions.size());//Add the symetric encryption functions number

            return ret;
        }

        bool SecuredTCPOptions::FindBestFunctions(const SecuredVector<byte> &parameters, AsymetricEncryption &asymetric, SymetricEncryption &symetric, HashFunction &hashfunction) const
        {
            if (parameters.size() < 3 || parameters.size() != parameters[1] + parameters[2] + parameters[3] + 4)//Check parameter's validity
            {
                return false;
            }

            //read parameters first
            if (parameters[0] > BSL_SECUREDTCPVERSION)//Check that we can handle the remote version of the procotol
            {
                return false;
            }

            int pos = 3;//Start from the first asymetrical algorithm
            vector<AsymetricEncryption> asym(parameters[1]);
            while (pos < 3 + parameters[1])//Adds asymetrical encryption functions
            {
                asym[pos - 3] = static_cast<AsymetricEncryption>(parameters[pos]);
            }

            vector<SymetricEncryption> sym(parameters[2]);
            while (pos < 3 + parameters[1] + parameters[2])//Add symetrical encryption functions
            {
                asym[pos - parameters[1] - 3] = static_cast<AsymetricEncryption>(parameters[pos]);
            }

            vector<HashFunction> hash(parameters[3]);

            while (pos < 3 + parameters[1] + parameters[2] + parameters[3])//Finally add hash functions
            {
                asym[pos - parameters[1] - parameters[2] - 3] = static_cast<AsymetricEncryption>(parameters[pos]);
            }

            asymetric = GetBestAsymetricEncryption(asym);//Find best matches
            symetric = GetBestSymetricEncryption(sym);
            hashfunction = GetBestHashFunction(hash);

            return asymetric != AsymetricEncryption::None && symetric != SymetricEncryption::None;//The hash function may be set to none
        }
    }
}