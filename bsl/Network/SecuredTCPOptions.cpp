/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "SecuredTCPOptions.h"
#include "../Crypto/CryptoProvider.h"
#define BSL_SECUREDTCPVERSION 1

using namespace BSL::Crypto;
using namespace BSL::Network::Helpers;

using namespace std;
namespace BSL
{
    namespace Network
    {
        SecuredTCPOptions::SecuredTCPOptions(bool defaultinit)
        {
            if (defaultinit)
            {
                AddAllowedEncryption(AsymetricEncryption::Default);
                AddAllowedEncryption(SymetricEncryption::Default);
            };
            _hashfunctions = { HashFunction::None };
        }

        void SecuredTCPOptions::AddAllowedEncryption(AsymetricEncryption alg, uint minsize, uint maxsize)
        {
            if (!CryptoProvider::IsValidFunction(alg))
            {
                throw Exception("Invalid cryptographic function");
            }

            if (minsize == 0 || maxsize == 0)//If both parameter are 0, take the default key size
            {
                if (minsize != maxsize)
                {
                    throw Exception("Invalid key size in SecuredTCPOptions::AddAllowedEncryption");
                }
                minsize = CryptoProvider::GetDefaultKeySize(alg);
                maxsize = minsize;
            }

            if (std::find_if(_asymetricalgs.begin(), _asymetricalgs.end(), [&](EncyptionOptions<AsymetricEncryption> &e)
            {
                if (e.algorithm == alg)//If the algorithm if found, update key sizes
                {
                    e.minkeysize = minsize;
                    e.maxkeysize = maxsize;
                    return true;
                }
                return false;
            }) == _asymetricalgs.end())//Add only if the value if not already in the vector
            {
                _asymetricalgs.push_back(EncyptionOptions<AsymetricEncryption>(alg, minsize, maxsize));
            }
        }

        void  SecuredTCPOptions::AddAllowedEncryption(SymetricEncryption alg, uint minsize, uint maxsize)
        {
            if (!CryptoProvider::IsValidFunction(alg))
            {
                throw Exception("Invalid cryptographic function");
            }

            if (minsize == 0 || maxsize == 0)//If both parameter are 0, take the default key size
            {
                if (minsize != maxsize)
                {
                    throw Exception("Invalid key size in SecuredTCPOptions::AddAllowedEncryption");
                }
                minsize = CryptoProvider::GetDefaultKeySize(alg);
                maxsize = minsize;
            }

            if (std::find_if(_symetricalgs.begin(), _symetricalgs.end(), [&](EncyptionOptions<SymetricEncryption> &e)
            {
                if (e.algorithm == alg)//If the algorithm if found, update key sizes
                {
                    e.minkeysize = minsize;
                    e.maxkeysize = maxsize;
                    return true;
                }
                return false;
            }) == _symetricalgs.end())//Add only if the value if not already in the vector
            {
                _symetricalgs.push_back(EncyptionOptions<SymetricEncryption>(alg, minsize, maxsize));
            }
        }

        void  SecuredTCPOptions::AddAllowedHashFunction(HashFunction alg)
        {
            if (!CryptoProvider::IsValidFunction(alg))
            {
                throw Exception("Invalid cryptographic function");
            }
            if (std::find(_hashfunctions.begin(), _hashfunctions.end(), alg) == _hashfunctions.end())//Add only if the value if not already in the vector
            {
                _hashfunctions.push_back(alg);
            }
        }

        void SecuredTCPOptions::SetSecret(const SecuredVector<byte> &secret)
        {
            if (std::find_if(_hashfunctions.cbegin(), _hashfunctions.cend(), [](const HashFunction &e)//Check that at least one hash function is accepted
            {
                return e != HashFunction::None;
            }) == _hashfunctions.cend())
            {
                throw Exception("SetSecret was called without a valid hash function set");
            }
            _secret = secret;
        }

        const SecuredVector<byte> &SecuredTCPOptions::GetSecret() const
        {
            return _secret;
        }

        vector<byte> SecuredTCPOptions::GetParameterPacket() const
        {
            //Data format: 
            //[UBYTE] Protocol version number
            //[UBYTE] Number of accepted asymetric encryptions
            //[UBYTE] Number of accepted symetric encryptions
            //[UBYTE] Number of accepted hash functions
            //[AlgData*] Data block
            //[UBYTE*]Hash functions data block
            //With:
            //AlgData=
            //[UBYTE] Algorithm identifyer
            //[UINT32] Minimal key size
            //[UINT32] Maximal key size

            std::vector<byte> ret;
            ret.resize(sizeof(byte) * 4 + (_asymetricalgs.size() + _symetricalgs.size()) *sizeof(AlgorithmParams) + _hashfunctions.size());//Avoid multiple allocations

            ParameterHeader *header = reinterpret_cast<ParameterHeader*>(ret.data());

            header->version = BSL_SECUREDTCPVERSION;//Set the version the version number
            header->asymetric = static_cast<byte>(_asymetricalgs.size());//Set the asymetric encryption functions number
            header->symetric = static_cast<byte>(_symetricalgs.size());//Set the symetric encryption functions number
            header->hash = static_cast<byte>(_hashfunctions.size());//Set the hash functions number


            uint pos = 0;
            for (const auto &e : _asymetricalgs)//Add asymetric encryption functions
            {
                header->algs[pos] = e;
                pos++;
            }

            for (const auto &e : _symetricalgs)//Add asymetric encryption functions
            {
                header->algs[pos] = e;
                pos++;
            }

            byte *hash = ret.data() + ret.size() - _hashfunctions.size();
            for (const auto &e : _hashfunctions)//Add hash functions
            {
                *hash = static_cast<byte>(e);
                hash++;
            }
            return ret;
        }

        bool SecuredTCPOptions::FindBestFunctions(const SecuredVector<byte> &parameters, AsymetricEncryption &asymetric, SymetricEncryption &symetric, HashFunction &hashfunction, uint &asymetrickeysize, uint &symetrickeysize) const
        {
            //Check minimal size validity
            if (parameters.size() < 4)
            {
                return false;
            }

            //Read header 
            const ParameterHeader *header = reinterpret_cast<const ParameterHeader*>(parameters.data());
            if (parameters.size() != (header->asymetric + header->symetric)*sizeof(AlgorithmParams) + header->hash + 4)//Verify header's size validity
            {
                return false;
            }

            //read parameters first
            if (header->version > BSL_SECUREDTCPVERSION)//Check that we can handle the remote version of the procotol
            {
                return false;
            }

            //Load accepted functions
            vector<EncyptionOptions<AsymetricEncryption>> asym(header->asymetric);
            uint pos = 0;//Start from the first asymetrical algorithm
            while (pos < header->asymetric)//Read asymetrical aglorithms
            {
                asym[pos] = header->algs[pos];
                pos++;
            }

            vector<EncyptionOptions<SymetricEncryption>> sym(header->symetric);
            while (pos < header->asymetric + header->symetric)//Read symetrical algorithms
            {
                sym[pos - header->asymetric] = header->algs[pos];
                pos++;
            }

            pos = pos * sizeof(AlgorithmParams) + 4;//Use the position to add hash functions now
            vector<HashFunction> hash;
            hash.reserve(header->hash);//Allocate memory only once
            while (pos < parameters.size())//Adds hash encryption functions
            {
                hash.push_back(static_cast<HashFunction>(parameters[pos]));
                pos++;
            }

            //Sort vectors to make sure prefered fonctions arrive firsts
            std::sort(asym.begin(), asym.end());
            std::sort(sym.begin(), sym.end());
            std::sort(hash.begin(), hash.end());

            //Find best matches
            EncyptionOptions<AsymetricEncryption> localasym;
            EncyptionOptions<AsymetricEncryption> remoteasym;
            if (!GetBestMatch(asym, _asymetricalgs, &localasym, &remoteasym))
            {
                return false;
            }

            EncyptionOptions<SymetricEncryption> localsym;
            EncyptionOptions<SymetricEncryption> remotesym;
            if (!GetBestMatch(sym, _symetricalgs, &localsym, &remotesym))
            {
                return false;
            }

            HashFunction hashalg;
            if (!GetBestMatch(hash, _hashfunctions, &hashalg))
            {
                return false;
            }

            hashfunction = hashalg;
            asymetric = localasym.algorithm;
            symetric = localsym.algorithm;
            //Take maximum encryption key sizes
            asymetrickeysize = std::min<uint>(localasym.maxkeysize, remoteasym.maxkeysize);
            symetrickeysize = std::min<uint>(localsym.maxkeysize, remotesym.maxkeysize);

            return true;
        }
    }
}