/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: TCPServer.h
//Purpose: Contains a struct that describtes a TCP client
#pragma once
#include <string>
#include <winsock.h>
namespace BSL
{
    namespace Network
    {
        struct ClientInfos
        {
            std::string hostname;//The adress of the client
            unsigned short port;//The high level TCP port
            SOCKET socket;
        };
    }
}