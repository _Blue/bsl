/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/
#include "SecuredTcp.h"
#include "../DataStructures/SecuredTCPMessageWriter.h"
#include "../DataStructures/SecuredTCPMessageReader.h"

#define TCP_MAX_PACKET_SIZE 64000000//TODO: move this in SecuredTCPOptions

using namespace BSL::Crypto;
using namespace BSL::DataStructures;
using namespace std;
namespace BSL
{
  namespace Network
  {
    SecuredTCP::SecuredTCP(const SecuredTCPOptions &options) : TCPBase()
    {
      _established = false;
      SetOptions(options);
    }

    SecuredTCP::SecuredTCP(SOCKET socket, const SecuredTCPOptions &options) : TCPBase(socket)
    {
      _established = false;
      SetOptions(options);
    }

    SecuredTCP::SecuredTCP(SecuredTCP &&other) : TCPBase(std::move(other))
    {
      _options = std::move(other._options);
      _datacryptor = std::move(_datacryptor);
      _hashfunction = std::move(_hashfunction);
    }

    void SecuredTCP::SetOptions(const SecuredTCPOptions &options)
    {
      _options = options;
    }

    const SecuredTCPOptions &SecuredTCP::GetOptions() const
    {
      return _options;
    }

    TCPError SecuredTCP::Send(const byte *data, size_t size)
    {
      SecuredVector<byte> datacopy(data, data + size);
      SecuredTCPMessageWriter writer(std::move(datacopy));//Create the message writer (call std::move to avoid a useless copy)
      SecuredVector<byte> message;
      try
      {
        message = writer.GetData(GetEncryptionFunction(), GetHashFunction(), _options.GetSecret());
      }
      catch (const BSL::Exception &)
      {
        return TCPError::EEncryptionError;
      }

      //we don't call Send(SecuredVector) because it would call the Send(byte*, size_t) function, 
      //which is overriden by us, in other words, it would cause a stack overflow
      return TCPBase::SendImpl(message.data(), message.size());
    }

    TCPError SecuredTCP::ReceivePacket(SecuredVector<byte> &data)
    {
      /*
      This function receive exactly one packet
      First it receive the 4 first bytes, to get the packet size
      then it polls the packet until the end
      */

      data.resize(sizeof(uint));
      TCPError ret = ReceiveExactly(data.data(), 4);//Receive 4 first bytes
      if (!ret)
      {
        return ret;
      }
      uint packetsize = *reinterpret_cast<uint*>(data.data());
      if (packetsize > TCP_MAX_PACKET_SIZE)//Check maximal allowed packet size
      {
        return TCPError::EPacketTooBig;
      }
      data.resize(packetsize);

      return ReceiveExactly(data.data() + sizeof(uint), packetsize - sizeof(uint));
    }

    TCPError SecuredTCP::Receive(SecuredVector<byte> &buffer, size_t)
    {
      TCPError ret = ReceivePacket(buffer);//Receive one whole packet
      if (!ret)
      {
        return ret;
      }
      SecuredTCPMessageReader reader(std::move(buffer));//Read the message
      TCPError error = reader.IsValid(GetHashFunction(), _options.GetSecret());//Checks its validity
      if (!error)
      {
        return error;
      }
      try
      {
        buffer = reader.GetData(GetEncryptionFunction(), GetHashFunction());
      }
      catch (const BSL::Exception &)
      {
        return TCPError::EEncryptionError;
      }
      return TCPError::Sucess;
    }

    TCPError SecuredTCP::ReceiveExactly(byte *data, uint size)
    {
      uint currentsize;
      while (size > 0)
      {
        currentsize = size;
        TCPError ret = ReceiveImpl(data, currentsize);
        if (!ret)
        {
          return ret;
        }
        size -= currentsize;
        data += currentsize;
      }
      return TCPError::Sucess;
    }

    TCPError SecuredTCP::NegociateParameters(AsymetricEncryption &exchangealg, uint &exchangekeysize, uint &datakeysize)
    {
      /*
      Both members send a parameter packet, which contains
      information about accepted encryption, key size etc...
      The objective here is to find the best encryption parameters
      as supported by both entities
      */

      auto localparameters = _options.GetParameterPacket();
      TCPError error = Send(localparameters);//First send our parameters
      if (!error)
      {
        return error;
      }

      SecuredVector<byte> remoteoptions;
      error = Receive(remoteoptions);//Then receive the remote parameters
      if (!error)
      {
        return error;
      }

      SymetricEncryption sym;
      HashFunction hash;
      if (!_options.FindBestFunctions(remoteoptions, exchangealg, sym, hash, exchangekeysize, datakeysize))//Find best parameters for both of us
      {
        return TCPError::ENegociationFailure;
      }

      _datacryptor = CryptoProvider::Construct(sym);//Construct the data encryptor
      if (hash != HashFunction::None)
      {
        _hashfunction = CryptoProvider::Construct(hash);//Construct the hash function manager
      }
      return TCPError::Sucess;
    }

    TCPError SecuredTCP::ExchangePublicKeys(unique_ptr<AsymetricEncryptor> &sender, unique_ptr<AsymetricEncryptor> &receiver, AsymetricEncryption alg, uint keysize)
    {
      /*
      Here we exchange our public key
      A sends his public key to B
      B sends his public key to A

      A can now talk to B using B's public key
      B can now talk to A using A's public key
      */

      //First receive build encryptor
      receiver = CryptoProvider::Construct(alg);

      //Then generate key
      if (!receiver->GenerateKey(keysize))
      {
        return TCPError::EEncryptionError;
      }

      //Export our public key
      SecuredVector<byte> localkey;
      if (!receiver->GetPublicKey(localkey))
      {
        return TCPError::EEncryptionError;
      }

      //Send our public key in a signed message
      SecuredTCPMessageWriter writer(localkey);
      try
      {
        auto publickeyblob = writer.GetData(nullptr, GetHashFunction(), _options.GetSecret());
        if (!Send(publickeyblob))
        {
          return TCPError::EMessageTransportFailure;
        }
      }
      catch (const BSL::Exception &)
      {
        return TCPError::EEncryptionError;
      }

      //Receive the sent public key
      SecuredVector<byte> keydata;
      if (!Receive(keydata))
      {
        return TCPError::EMessageTransportFailure;
      }

      //Build the packet sender
      sender = CryptoProvider::Construct(alg);
      SecuredTCPMessageReader reader(keydata);
      TCPError validity = reader.IsValid(GetHashFunction(), _options.GetSecret());
      if (!validity)
      {
        return validity;
      }
      try
      {
        if (!sender->SetPublicKey(reader.GetData(nullptr, GetHashFunction())))
        {
          return TCPError::EEncryptionError;
        }
      }
      catch (const BSL::Exception&)
      {
        return TCPError::EEncryptionError;
      }

      return TCPError::Sucess;
    }

    TCPError SecuredTCP::CreateDataKey(unique_ptr<AsymetricEncryptor> &sender, unique_ptr<AsymetricEncryptor> &receiver, uint keysize)
    {
      /*
      Now that have exchanged our public keys,
      both members send a random data blob of size keysize
      theses two blobs are XORed.
      The result of the XOR is derivated to become the symetric encryption key
      */

      //Generate our blob
      SecuredVector<byte> localblob(keysize);
      if (!sender->GetRandom(localblob))
      {
        return TCPError::EEncryptionError;
      }

      //Send our blob
      SecuredTCPMessageWriter writer(localblob);
      try
      {
        auto encodedblob = writer.GetData(nullptr, GetHashFunction(), _options.GetSecret());
        if (!Send(encodedblob.data(), encodedblob.size()))
        {
          return TCPError::EMessageTransportFailure;
        }
      }
      catch (const BSL::Exception&)
      {
        return TCPError::EEncryptionError;
      }

      //Get the remote blob
      SecuredVector<byte> packet;
      if (!Receive(packet, keysize))
      {
        return TCPError::EMessageTransportFailure;
      }

      //Verify packet signature
      SecuredTCPMessageReader reader(packet);
      TCPError validity = reader.IsValid(GetHashFunction(), _options.GetSecret());
      if (!validity)
      {
        return validity;
      }

      //Decrypt the remote blob
      SecuredVector<byte> remoteblob;
      try
      {
        remoteblob = reader.GetData(nullptr, GetHashFunction());
      }
      catch (const BSL::Exception &)
      {
        return TCPError::EEncryptionError;
      }

      //Check blob's size
      if (remoteblob.size() != keysize)
      {
        return TCPError::EProtocolMismatch;
      }

      //Create the session key
      if (!CreateSessionKey(localblob, remoteblob, keysize))
      {
        return TCPError::EEncryptionError;
      }

      _established = true;//Now the connexion is secured
      return TCPError::Sucess;
    }

    bool SecuredTCP::CreateSessionKey(const SecuredVector<byte> &localblob, const SecuredVector<byte> &remoteblob, uint keysize)
    {
      /*
      Here we XOR the two blob to obtain a final blob
      which will be derivated to get the final symetric encryption key
      localblob and remoteblob MUST have the same size
      */

      SecuredVector<byte> finalblob(localblob.size());
      for (uint i = 0; i < localblob.size(); i++)
      {
        finalblob[i] = localblob[i] ^ remoteblob[i];//XOR each byte
      }

      //Derivate the key from the XORed blob (the encryptor has been built in NegociateParameters earlier)
      return _datacryptor->DeriveKey(finalblob, keysize);
    }

    TCPError SecuredTCP::EstablishContext()
    {
      /*
      This function negociate parameters,
      then exchange public keys
      then generate the session key
      using a XOR of a local and remote data blob
      */


      //Negociate parameters
      AsymetricEncryption exchangealg;
      uint exchangekeysize;
      uint datakeysize;
      TCPError ret = NegociateParameters(exchangealg, exchangekeysize, datakeysize);
      if (!ret)
      {
        return ret;
      }

      //Exchange public keys
      unique_ptr<AsymetricEncryptor> exchangesender;
      unique_ptr<AsymetricEncryptor> exchangereceiver;
      ret = ExchangePublicKeys(exchangesender, exchangereceiver, exchangealg, exchangekeysize);
      if (!ret)
      {
        return ret;
      }

      //Create the session key
      return CreateDataKey(exchangesender, exchangereceiver, datakeysize);
    }

    HashBase *SecuredTCP::GetHashFunction() const
    {
      if (_options.GetSecret().empty())//Empty secret means no signature
      {
        return nullptr;
      }
      return _hashfunction.get();
    }

    SymetricEncryptor *SecuredTCP::GetEncryptionFunction() const
    {
      if (_established)
      {
        return _datacryptor.get();
      }
      return nullptr;
    }
  }
}