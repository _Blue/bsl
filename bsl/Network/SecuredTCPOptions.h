/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: SecuredTCPOptions
//Purpose: Class that contains option for a Secured TCP connection

#pragma 
#include <algorithm>
#include "../DataStructures/SecuredVector.h"
#include "../Tools/Exception.h"
#include "../BSLDefs.h"
#include "SecuredTCPOptionsHelpers.h"
namespace BSL
{
    namespace Crypto
    {
        enum class AsymetricEncryption : unsigned char;
        enum class SymetricEncryption : unsigned char;
        enum class HashFunction : unsigned char;
    }

    namespace Network
    {
        class SecuredTCPOptions
        {
        public:
            SecuredTCPOptions(bool defaultinit = true);//If defaultinit is true, default values will be loaded

            //These functions add an algorithm to the list of accepted ones
            //Algorithms added first will have priority over those that were added last
            void AddAllowedEncryption(Crypto::AsymetricEncryption alg, uint minkeysize = 0, uint maxkeysize = 0);
            void AddAllowedEncryption(Crypto::SymetricEncryption alg, uint minkeysize = 0, uint maxkeysize = 0);
            void AddAllowedHashFunction(Crypto::HashFunction alg);

            //Secret accessors, a secret can be set only if the HashFuntion is not HashFunction::None
            void SetSecret(const SecuredVector<byte> &secret = {});
            const SecuredVector<byte> &GetSecret() const;

            //returns a packet representing the supported protocols
            std::vector<unsigned char> GetParameterPacket() const;

            //Get the best functions, returns false if not match is found
            bool FindBestFunctions(const SecuredVector<byte> &parameters, Crypto::AsymetricEncryption &asymetric, Crypto::SymetricEncryption &symetric, Crypto::HashFunction &hash, uint &asymkeysize, uint &symkeysize) const;

        private:
            std::vector<Helpers::EncyptionOptions<Crypto::AsymetricEncryption>> _asymetricalgs;
            std::vector<Helpers::EncyptionOptions<Crypto::SymetricEncryption>> _symetricalgs;
            std::vector<Crypto::HashFunction> _hashfunctions;
            SecuredVector<byte> _secret;
        };
    }
}