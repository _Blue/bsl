/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#pragma once

//File: SecuredTCPClient.h
//Purpose: Provide encryption and signing layer for TCP communications client
#include "SecuredTcp.h"
#include "TCPClient.h"
namespace BSL
{
    namespace Network
    {
        class SecuredTCPClient : public SecuredTCP
        {
        public:
            SecuredTCPClient(const SecuredTCPOptions &options);
            SecuredTCPClient(const SecuredTCPClient &) = delete;
            SecuredTCPClient(SecuredTCPClient &&client);


            TCPError Connect(const std::string &host, ushort port);//Connects and securizes the connection
            TCPError Connect(SOCKET socket);//Securize a connection already established
        };
    }
}