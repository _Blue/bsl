/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: TCPServer.h
//Purpose: Contains class suitable to serve incomming TCP Connections
#pragma once
#include "TCPBase.h"
#include "ClientHandler.h"
#include "TCPError.h"
namespace BSL
{
    namespace Network
    {
        class TCPServer : protected TCPBase
        {
        public:
            TCPServer();
            TCPError Start(ushort port, ClientHandler &handler, const std::string &adress = "");//Blocking call, will call hander->ClientConnected() when a client connects. Stops when ClientConnected() returns true
        };
    }
}