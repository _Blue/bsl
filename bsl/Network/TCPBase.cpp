/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include <atomic>
#include "TCPBase.h"
using namespace std;
namespace BSL
{
    namespace Network
    {
        TCPBase::TCPBase(SOCKET socket)
        {
            _socket = socket;
            if (!InitWinsock(socket == 0))
            {
                throw Exception("Failed to initialize Winsock");
            }
        }

        TCPBase::TCPBase(TCPBase &&other)
        {
            _socket = other._socket;
            other._socket = 0;//Make sure that the socket won't be closed when the object is destroyed
            if (!InitWinsock(socket == 0))//Because to destructor of "other" will call WSACleanup
            {
                throw Exception("Failed to initialize Winsock");
            }
        }

        TCPBase::~TCPBase()
        {
            /*
            Because WSAStartup and WSACleanup have an internal counter,
            each call to WSAStartup must have imply a call to WSACleanup
            Only the last call to WSACleanup actually release ressources
            */
            WSACleanup();
            Close();
        }

        TCPError TCPBase::SendImpl(const byte *data, size_t datasize)
        {
            ULONG sent = 0;
            int ret;
            while (sent < datasize)//Sometimes, a single call to ::send in not enough to sent everything, doing this ensure ensures everything is sent
            {
                ret = send(_socket, reinterpret_cast<const char*>(data), datasize - sent, 0);
                if (ret <= 0)//Check for send error
                {
                    return TCPError::EConnectionClosed;
                }
                sent += ret;
            }
            return TCPError::Sucess;
        }

        TCPError TCPBase::Send(const string &data)
        {
            return Send(reinterpret_cast<const byte *>(data.data()), data.size());
        }

        TCPError TCPBase::Send(const SecuredVector<byte> &data)
        {
            return Send(data.data(), data.size());
        }

        TCPError TCPBase::Send(const byte *data, size_t datasize)
        {
            return SendImpl(data, datasize);
        }

        TCPError TCPBase::ReceiveImpl(byte *data, uint &size)
        {
            int ret = recv(_socket, reinterpret_cast<char*>(data), size, 0);
            if (ret <= 0)
            {
                return TCPError::EConnectionClosed;
            }
            size = ret;
            return TCPError::Sucess;
        }

        TCPError TCPBase::Receive(SecuredVector<byte> &data, size_t datasize)
        {
            data.resize(datasize);
            return ReceiveImpl(data.data(), datasize);
        }

        string TCPBase::ReceiveStr(size_t buffersize)
        {
            SecuredVector<byte> data;
            if (!Receive(data, buffersize))
            {
                return "";
            }
            return string(reinterpret_cast<const char*>(data.data()), data.size());
        }

        TCPError TCPBase::InitWinsock(bool createsocket)
        {
            //Call WSAStartup and check that the requested winsock version could be loaded (we want 2.2)
            WSADATA data;
            if (WSAStartup(MAKEWORD(2, 2), &data) || LOBYTE(data.wVersion) != 2 || HIBYTE(data.wVersion) != 2)
            {
                return TCPError::EWSAError;
            }


            if (createsocket)
            {
                //Create a socket for TCP
                _socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
                if (_socket == INVALID_SOCKET)
                {
                    return TCPError::EWSAError;
                }
            }

            return TCPError::Sucess;
        }

        void TCPBase::Close()
        {
            if (_socket != 0)
            {
                closesocket(_socket);
                _socket = 0;
            }
        }

        SOCKET TCPBase::ExtractSocket()
        {
            if (_socket == 0)
            {
                throw Exception("TCPBase::ExtractSocket was called on an instance that has no connected SOCKET");
            }

            SOCKET ret = _socket;
            _socket = 0;//Set the socket to zero so that the socket does no get closed when the object is destroyed
            return ret;
        }

        ushort TCPBase::GetHighConnectionPort()
        {
            sockaddr_in addr;
            ZeroMemory(&addr, sizeof(addr));
            int size = sizeof(addr);
            if (getsockname(_socket, reinterpret_cast<sockaddr*>(&addr), &size))
            {
                return 0;
            }
            return ntohs(addr.sin_port);
        }
    }
}