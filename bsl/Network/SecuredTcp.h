/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#pragma once

//File: SecuredTCP.h
//Purpose: Provide encryption and signing layer for TCP communications
#include <memory>
#include "TCPBase.h"
#include "SecuredTCPOptions.h"
#include "../Crypto/CryptoProvider.h"
namespace BSL
{
    namespace Network
    {
        class SecuredTCP : public TCPBase
        {
        public:
            SecuredTCP(const SecuredTCPOptions &options = SecuredTCPOptions());
            SecuredTCP(SOCKET socket, const SecuredTCPOptions &options = SecuredTCPOptions());

            SecuredTCP(const SecuredTCP&) = delete;
            SecuredTCP(SecuredTCP &&other);

            //Options accessors, set cryptographic options, such as encryption algorithm and message signing algorithms
            void SetOptions(const SecuredTCPOptions &options = SecuredTCPOptions());
            const SecuredTCPOptions &GetOptions() const;

            //Send receive functions
            TCPError Send(const byte *data, size_t datasize) override;
            TCPError Receive(SecuredVector<byte> &buffer, size_t datasize = 0) override;//receives a packet, datasize is ignore here

            //Import base class functions
            using TCPBase::Send;
            using TCPBase::Receive;

        protected:
            TCPError EstablishContext();//Establishes secured context, negociates options and exchanges public keys
        
        private:
            TCPError ReceivePacket(SecuredVector<byte> &data);
            TCPError ReceiveExactly(byte *data, uint size);//Blocks until "size" bytes are received or error
            Crypto::HashBase *GetHashFunction() const;
            Crypto::SymetricEncryptor *GetEncryptionFunction() const;
            TCPError NegociateParameters(Crypto::AsymetricEncryption &exchangealg, uint &exchangekeysize, uint &datakeysize);//Exchange SecuredTCPOptions, and negociates bests available algorithm
            TCPError ExchangePublicKeys(std::unique_ptr<Crypto::AsymetricEncryptor> &sender, std::unique_ptr<Crypto::AsymetricEncryptor> &receiver, Crypto::AsymetricEncryption alg, uint keysize);//Exchange public keys
            TCPError CreateDataKey(std::unique_ptr<Crypto::AsymetricEncryptor> &sender, std::unique_ptr<Crypto::AsymetricEncryptor> &receiver, uint keysize);//Create symetric key for session
            bool CreateSessionKey(const SecuredVector<byte> &localblob, const SecuredVector<byte> &remoteblob, uint keysize);

            SecuredTCPOptions _options;
            std::unique_ptr<Crypto::SymetricEncryptor> _datacryptor;
            std::unique_ptr<Crypto::HashBase> _hashfunction;
            bool _established;//True when the secured context is established
        };
    }
}