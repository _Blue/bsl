/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or not, open source or not.                                 *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/

#include "SecuredTCPClient.h"
#include "TCPClient.h"
using namespace std;
namespace BSL
{
    namespace Network
    {
        SecuredTCPClient::SecuredTCPClient(const SecuredTCPOptions &options) : SecuredTCP(options)
        {

        }

        SecuredTCPClient::SecuredTCPClient(SecuredTCPClient &&other) : SecuredTCP(std::move(other))
        {

        }

        TCPError SecuredTCPClient::Connect(const string &host, ushort port)
        {
            TCPClient client;
            TCPError ret = client.Connect(host, port);//Connect the SOCKET using a TCPClient
            if (!ret)
            {
                return ret;
            }
            _socket = client.ExtractSocket();//Take the SOCKET from the client

            return EstablishContext();
        }

        TCPError SecuredTCPClient::Connect(SOCKET socket)
        {
            _socket = socket;//Take the already connected socket
            return EstablishContext();
        }
    }
}