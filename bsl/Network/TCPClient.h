/**********************************************************************************
*This file is part of the Blue Software Library (bsl).                            *
*The bsl is being developed by Blue Software (http://bluecode.fr).                *
*                                                                                 *
*You are allowed to use, modify and redistribute the bsl in any project           *
*you want, commercial or note, open source or not.                                *
*                                                                                 *
*The bsl is being distributed by Blue Software without any guarantee of any kind, *
*you use it at your own risks                                                     *
*                                                                                 *
*Blue Software Library, Blue, 2015                                                *
***********************************************************************************/


//File: TCPClient.h
//Purpose: Contains TCPClient class, provides TCP connection and inherits of TCPBase
#pragma once

#include "TCPBase.h"
#include "TCPError.h"
namespace BSL
{
    namespace Network
    {
        class TCPClient : public TCPBase
        {
        public:
            TCPClient(SOCKET socket = 0);
            virtual TCPError Connect(const std::string &host, ushort port);
        };
    }
}